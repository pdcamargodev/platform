export function arrayHasIndex<T>(array: T[], index: number): boolean {
	return Array.isArray(array) && array.hasOwnProperty(index)
}

export function editArrayByIndex<T>(array: T[], index: number, newItem: T) {
	const newArray = array;
	if(arrayHasIndex<T>(newArray, index) && newItem) {
		newArray[index] = newItem
	}

	return newArray;
}
