export function removeAllWhiteSpace(str: string) {
	return str.replace(/\s/g,'');
}
