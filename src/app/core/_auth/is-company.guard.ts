import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../_services';

@Injectable({
  providedIn: 'root'
})
export class IsCompanyAuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let hasCurrentUser = this.authService.getCurrentUser() != null;
      let isCurrentUserCompany = hasCurrentUser && this.authService.getCurrentUser().userType == "company";

      let exceptions = [];

      let isOnExceptions = exceptions.includes(state.url);

      let can = (hasCurrentUser && isCurrentUserCompany) || isOnExceptions;

      if(!can) {
        this.router.navigate(['/auth/empresa/entrar']);
      }

      return can;
    }
}
