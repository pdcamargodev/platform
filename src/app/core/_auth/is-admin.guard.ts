import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../_services';

@Injectable({
  providedIn: 'root'
})
export class IsAdminAuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let hasCurrentUser = this.authService.getCurrentUser() != null;
      let isCurrentUserAdmin = hasCurrentUser && this.authService.getCurrentUser().userType == "admin";

      let exceptions = [];

      let isOnExceptions = exceptions.includes(state.url);

      let can = (hasCurrentUser && isCurrentUserAdmin) || isOnExceptions;

      if(!can) {
        this.router.navigate(['/auth/administrador/entrar']);
      }

      return can;
    }
}
