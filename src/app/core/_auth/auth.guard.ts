import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../_services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let hasCurrentUser = this.authService.getCurrentUser() != null;

      let exceptions = [];

      let isOnExceptions = exceptions.includes(state.url);

      let can = hasCurrentUser || isOnExceptions;

      if(!can) {
        this.router.navigate(['/auth/entrar']);
      }

      return can;
    }
}