import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../_services';

@Injectable({
  providedIn: 'root'
})
export class IsFreelancerAuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let hasCurrentUser = this.authService.getCurrentUser() != null;
      let isCurrentUserFreelancer = hasCurrentUser && this.authService.getCurrentUser().userType == "freelancer";

      let exceptions = [];

      let isOnExceptions = exceptions.includes(state.url);

      let can = (hasCurrentUser && isCurrentUserFreelancer) || isOnExceptions;

      if(!can) {
        this.router.navigate(['/auth/prestador/entrar']);
      }

      return can;
    }
}
