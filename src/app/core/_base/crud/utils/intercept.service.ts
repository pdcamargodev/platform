// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../../../core/_services';
import { environment } from '../../../../../environments/environment';
import { Router } from '@angular/router';

@Injectable()
export class InterceptService implements HttpInterceptor {
	constructor(
		private authService: AuthService,
		private router: Router
	) {}
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		if(this.authService.getCurrentUser() != null) {
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ${this.authService.getCurrentUserToken()}`
				}
			});
		}

		return next.handle(request).pipe(
			tap(
				event => {
					 if (event instanceof HttpResponse) {
						//  Things were fine
					}
				},
				(error: HttpErrorResponse) => {
					if(error.status == 401 && (
						error.error != null &&
						error.error.error != null &&
						error.error.error == "Unauthorized" &&
						error.error.statusCode != null &&
						error.error.statusCode == 401) &&
						!this.router.url.includes('auth')
					) {
						let user = this.authService.getCurrentUser();

						if(user == null) {
							this.router.navigateByUrl('/auth/entrar');
							this.authService.logout();
							return
						}

						let route: string;
						if(user.userType == 'freelancer') {
							route = "/auth/entrar";
						} else if(user.userType == 'company') {
							route = "/auth/empresa/entrar";
						} else if(user.userType == 'admin') {
							route = "/auth/admin/entrar";
						}

						this.authService.logout();
						this.router.navigateByUrl(route);
					} else if(!environment.production) {
						console.group('----request error----');
						console.error('status', error.status);
						console.error('error', error.message);
						console.groupEnd();
					}
				}
			)
		);
	}
}
