import { FreelancerProfile } from './freelancer-profile.model';
import { FreelancerBankAccount } from './freelancer-bank-account.model';
import { FreelancerTimeAvailable } from './freelancer-time-available.model';
import { FreelancerFinancial } from './freelancer-financial.model';

export interface Freelancer {
    id?: number;
    username?: string;
    email?: string;
    password?: string;
    valid?: number;
    slug?: string;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date|null;
    // Rleations
    profile?: FreelancerProfile;
    financial?: FreelancerFinancial;
    bank_account?: FreelancerBankAccount;
    time_available?: FreelancerTimeAvailable;
}