export interface FreelancerNotification {
    id?: number;
    freelancer_id?: number;
    viewed?: number;
    title?: string;
    description?: string;
    go_to?: string;
    created_at?: Date;
    notified_at?: Date;
    deleted_at?: Date;
}