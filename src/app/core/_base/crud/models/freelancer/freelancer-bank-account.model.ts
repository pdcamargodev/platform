export interface FreelancerBankAccount {
    id?: number,
    freelancer_id?: number,
    bank?: string,
    agency?: string,
    account?: string,
    full_name?: string,
    cpf?: string,
    account_type?: 'savings'|'checking',
    created_at?: Date,
    updated_at?: Date|null,
    deleted_at?: Date|null
}