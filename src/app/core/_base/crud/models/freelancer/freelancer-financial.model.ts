export interface FreelancerFinancial {
    id?: number;
    freelancer_id?: number;
    month_earning?: number;
    current_hourly_value?: number;
    hour_value_progress?: number;
    next_hourly_value_analysis?: Date|null;
    updated_at?: Date|null;
}