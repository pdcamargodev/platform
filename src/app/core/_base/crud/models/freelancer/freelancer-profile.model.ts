export interface FreelancerProfile {
    id?: number;
    freelancer_id?: number;
    name?: string;
    last_name?: string;
    avatar?: string|null;
    birthday?: Date;
    job?: string|null;
    area?: string;
    cellphone?: string;
    location?: string;
    updated_at?: string|null;
}