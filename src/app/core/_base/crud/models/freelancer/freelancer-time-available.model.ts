export interface FreelancerTimeAvailable {
    id?: number,
    freelancer_id?: number,
    monday?: number,
    tuesday?: number,
    wednesday?: number,
    thursday?: number,
    friday?: number,
    saturday?: number,
    sunday?: number,
    updated_at?: Date|null,
    requested_analysis_at?: Date|null,
    can_update_time_available: number
}