import { Product } from '../product';

export interface PackageProduct {
    id?: number;
    product_id?: number;
    package_id?: number;
    quantity?: number;
    created_at?: Date;
    deleted_at?: Date|null;

    // Relations
    product?: Product;
}