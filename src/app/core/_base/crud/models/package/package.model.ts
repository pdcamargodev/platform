import { PackageProduct } from './package-product.model';

export interface Package {
    id?: number;
    name?: string;
    description?: string;
    image?: string;
    price_coin?: number;
    price?: number;
    created_at?: Date;
    updated_at?: null|Date;
    deleted_at?: null|Date;
    allowed_payment_type?: 'bank_slip'|'credit_card'|'all';

    // Relations
    package_product?: PackageProduct[];
}
