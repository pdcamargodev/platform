export interface FilterFreelancerJobsDto {
    companies?: number[];
    order_by: 'most_recent'|'less_recent'|'company'|'more_finish_date'|'less_finish_date';
}