export interface FilterFreelancerDto {
    name?: string;
    area?: 'designer'|'developer'|'editor';
}