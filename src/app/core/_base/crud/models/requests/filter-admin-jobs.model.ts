export interface FilterAdminJobsDto {
    companies?: number[];
    freelancers?: number[];
    order_by: 'most_recent'|'less_recent'|'company'|'more_finish_date'|'less_finish_date';
}