export type InvoiceItem = {
	item_type: 'product'|'package'|'plan';
	item_id: number;
	quantity: number;
	payment_type: 'bank_slip'|'credit_card'|'coin';
}
export type Document = {
	type: string;
	number: string;
}
export interface CreateCompanyInvoice {
	price: number;
	invoice_item: InvoiceItem[];
	pagarme_info: {
		customer: {
			name: string;
			type: 'individual'|'corporation';
			country: string;
			email: string;
			documents: Document[];
			phone_numbers: string[];
			birthday: string;
		};
		billing: {
			name: string;
			address: {
				name: string;
				street: string;
				street_number: string;
				zipcode: string;
				country: string;
				state: string;
				city: string;
				neighborhood: string;
				complementary?: string;
			}
		};
		card?: {
			card_number: string;
			card_cvv: string;
			card_expiration_date: string;
			card_holder_name: string;
		};
		card_id?: string;
		bank_slip?: {
			boleto_expiration_date: string;
			boleto_instructions: string;
		};
		installments: '1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'10'|'11'|'12';
		payment_method: 'boleto'|'credit_card';
		postback_url: string;
	}
}
