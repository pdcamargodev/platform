import { BaseResponseInterface } from '.';
import { FreelancerNotification } from '../freelancer';

export interface GetFreelancerNotificationResponse extends BaseResponseInterface {
    data: FreelancerNotification[]
}