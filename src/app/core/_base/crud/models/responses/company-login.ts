import { CompanyUser } from '../company/company-user.model';
import { BaseResponseInterface } from './base-response';

export interface CompanyLogin {
    access_token: string;
    company_user: CompanyUser
}

export interface CompanyLoginResponse extends BaseResponseInterface {
    data: CompanyLogin
}