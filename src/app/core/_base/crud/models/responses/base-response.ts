export interface BaseResponseInterface {
    error: boolean;
    message?: string;
}

export interface BaseResponse extends BaseResponseInterface {
    data?: any;
}