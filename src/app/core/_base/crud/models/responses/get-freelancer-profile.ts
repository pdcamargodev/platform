import { BaseResponseInterface } from '.';
import { Freelancer } from '../freelancer';

export interface GetFreelancerProfile extends BaseResponseInterface {
    data: Freelancer|null
}