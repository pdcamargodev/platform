import { BaseResponseInterface } from '.';
import { JobAttachment } from '../job';

export interface JobAttachmentResponse extends BaseResponseInterface {
    data: JobAttachment
}