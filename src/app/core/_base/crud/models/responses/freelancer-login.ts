import { BaseResponseInterface } from '.';
import { Freelancer } from '../freelancer';

export interface FreelancerLogin {
    access_token: string;
    freelancer: Freelancer
}

export interface FreelancerLoginResponse extends BaseResponseInterface {
    data: FreelancerLogin
}