import { Job } from './../job/job.model';
import { BaseResponseInterface } from '.';

export interface GetCompanyJobs extends BaseResponseInterface {
    data: Job[]
}
