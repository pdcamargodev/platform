import { BaseResponseInterface } from '.';
import { Job } from '../job';

export interface GetAdminJobsResponse extends BaseResponseInterface {
    data: Job[]
}