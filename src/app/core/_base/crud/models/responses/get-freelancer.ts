import { BaseResponseInterface } from '.';
import { Freelancer } from '../freelancer';

export interface GetFreelancerResponse extends BaseResponseInterface {
    data: Freelancer[]
}