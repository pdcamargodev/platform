import { BaseResponseInterface } from '.';
import { Package } from '../package';

export interface GetPackagesResponse extends BaseResponseInterface {
    data: Package[]
}