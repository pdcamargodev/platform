import { BaseResponseInterface } from '.';
import { JobCommentary } from '../job';

export interface CreateCommentaryResponse extends BaseResponseInterface {
    data: JobCommentary
}