import { BaseResponseInterface } from '.';
import { Plan } from '../plan';

export interface GetPlansResponse extends BaseResponseInterface {
    data: Plan[]
}