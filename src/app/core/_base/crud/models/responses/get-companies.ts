import { BaseResponseInterface } from '.';
import { Company } from '../company';

export interface GetCompaniesResponse extends BaseResponseInterface {
    data: Company[]
}