import { BaseResponseInterface } from '.';
import { Admin } from '../admin';

export interface AdminLogin {
    access_token: string;
    admin: Admin
}

export interface AdminLoginResponse extends BaseResponseInterface {
    data: AdminLogin
}