import { BaseResponseInterface } from '.';
import { CompanyBillingAddress } from './../company/company-billing-address.model';

export interface GetCompanyBillingAddress extends BaseResponseInterface {
    data: CompanyBillingAddress[]
}
