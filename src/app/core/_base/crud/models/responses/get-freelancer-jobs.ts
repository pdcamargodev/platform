import { BaseResponseInterface } from '.';
import { Job } from '../job';

export interface GetFreelancerJobsResponse extends BaseResponseInterface {
    data: Job[]
}