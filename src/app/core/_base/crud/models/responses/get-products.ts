import { BaseResponseInterface } from '.';
import { Product } from '../product';

export interface GetProductsResponse extends BaseResponseInterface {
    data: Product[]
}