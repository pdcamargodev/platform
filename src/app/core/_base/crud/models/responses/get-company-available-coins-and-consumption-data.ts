import { BaseResponseInterface } from '.';

export interface CompanyAvailableCoinsAndConsumptionData {
    package: number
    package_percentage: number
    development: number
    development_percentage: number
    social_media: number
    social_media_percentage: number
    strategy: number
    strategy_percentage: number
    available_coins: number
}

export interface GetCompanyAvailableCoinsAndConsumptionDataResponse extends BaseResponseInterface {
    data: CompanyAvailableCoinsAndConsumptionData
}