export interface JobAttachment {
    id?: number;
    job_id?: number;
    sender_id?: number;
    sender_type?: 'admin'|'freelancer'|'company';
    name?: string;
    url?: string;
    mime_type?: string;
    bytes?: number;
    created_at?: Date;
}