import { Admin } from '../admin';
import { Freelancer } from '../freelancer';
import { CompanyUser } from '../company';

export interface JobCommentary {
    id?: number;
    job_id?: number;
    sender_id?: number;
    sender_type?: 'admin'|'freelancer'|'company';
    commentary?: string;
    created_at?: Date;
    updated_at?: Date|null;
    deleted_at?: Date|null;
    //Relations
    admin?: Admin;
    freelancer?: Freelancer;
    company_user?: CompanyUser
}