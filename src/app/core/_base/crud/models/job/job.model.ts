import { CompanyJobFeedback } from './../company/company-job-feedback.model';
import { CompanyJobFeedbackRequest } from './../company/company-job-feedback-request.model';
import { JobCommentary } from './job-commentary.model'
import { Company } from '../company'
import { JobAttachment } from './job-attachment.model'
import { Freelancer } from '../freelancer'

export interface Job {
    id?: number
    company_id?: number
    company_product_id?: number
    freelancer_id?: number
    title?: string
    notes?: string
    status?: 'to_do'|'doing'|'to_approval'|'to_change'|'done'
    paused?: 0|1
    start_date?: Date|null
	finish_date?: Date|null
    started_at?: Date|null
    finished_at?: Date|null
    created_at?: Date
    updated_at?: Date
	deleted_at?: Date|null
	// Dynamic data
    approved_at?: Date|null
    disapproved_at?: Date|null
    // Relations
    company?: Company
    commentary?: JobCommentary[]
    freelancer?: Freelancer
	attachment?: JobAttachment[]
	company_feedback_request?: CompanyJobFeedbackRequest[]
	company_feedback?: CompanyJobFeedback[]
}
