import { CardComment } from './card-comment.model';

export interface Card {
    title: string;
    description?: string;
    comments?: CardComment[];
}