import { Freelancer } from '../freelancer';

export interface CardComment {
  comment: string;
  user: Freelancer;
  created_at: Date;
  answers?: CardComment[];
}