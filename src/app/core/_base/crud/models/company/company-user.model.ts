export interface CompanyUser {
    id?: number;
    company_id?: number;
    owner?: number;
    permission?: string;
    email?: string;
    name?: string;
    last_name?: string;
    avatar?: string;
    password?: string;
    slug?: string;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date|null;
}