export interface CompanyBillingAddress {
    id?: number
	company_id?: number
	full_name?: string
	cellphone?: string
	zip_code?: string
	street?: string
	number?: string
	complement?: string
	neighborhood?: string
	city?: string
	state?: string
    created_at?: Date
}
