import { CompanyJobFeedback } from './company-job-feedback.model';
export interface CompanyJobFeedbackRequest {
	id?: number
	job_id?: number
	notes?: string
	created_at?: Date
	company_id?: number

	// Relations
	feedback?: CompanyJobFeedback
}
