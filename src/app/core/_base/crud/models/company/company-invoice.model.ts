export interface CompanyInvoice {
    id?: number
    company_id?: number
    payment_type?: 'bank_slip'|'credit_card'|'coin'
    payment_status?: 'processing'|'authorized'|'paid'|'refunded'|'waiting_payment'|'pending_refund'|'refused'
    item_id?: number
    quantity?: number
    created_at?: Date
    updated_at?: null|Date
    deleted_at?: null|Date
    item_type: 'product'|'package'|'plan'|'coin'
    price?: number
    discount?: number
}