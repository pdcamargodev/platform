export interface CompanyJobFeedback {
	id?: number
	job_id?: number
	company_user_id?: number
	company_job_feedback_request_id?: number
	status?: 'approval'|'disapproval'
	feedback?: string
	created_at?: Date
}
