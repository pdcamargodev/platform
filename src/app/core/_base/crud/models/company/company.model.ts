import { CompanyInvoice } from './company-invoice.model';
import { CompanyUser } from './company-user.model';

export interface Company {
    id?: number
    name?: string
    cellphone?: string
    telephone?: string
    segment?: string
    site?: string
    slug?: string
    logo?: string
    localization?: string
    cnpj?: string
    valid?: 0|1
    created_at?: Date
    updated_at?: Date|null
    deleted_at?: Date|null

    // Relations
    invoice?: CompanyInvoice[]
    users?: CompanyUser[]
}