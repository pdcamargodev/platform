export interface Admin {
    id?: number;
    avatar?: string;
    name?: string;
    last_name?: string;
    email?: string;
    created_at?: Date;
    deleted_at?: Date|null;
    password?: string;
}