export interface Product {
    id?: number;
    name?: string;
    category?: 'social_media'|'strategy'|'development';
    allowed_payment_type?: 'bank_slip'|'credit_card'|'all';
    description?: string;
    image?: string;
    price_coin?: number;
    price?: number;
    created_at?: Date;
    updated_at?: null|Date;
    deleted_at?: null|Date;
}