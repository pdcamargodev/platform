import { PlanProduct } from './plan-product.model';

export interface Plan {
    id?: number;
    name?: string;
    description?: string;
    image?: string;
    price?: number;
    allowed_payment_type?: 'bank_slip'|'credit_card'|'all';
    charges?: number;
    pagarme_plan_id?: number;
    created_at?: Date;
    updated_at?: Date|null;
    deleted_at?: Date|null;

    // Relations
    plan_product?: PlanProduct[];
}
