import { Product } from '../product';
import { Package } from '../package';

export interface PlanProduct {
    id?: number;
    plan_id?: number;
    item_id?: number;
    item_type?: 'product'|'package';
    quantity?: number;
    created_at?: Date;
    deleted_at?: Date|null;

    // Relations
    product?: Product;
    package?: Package;
}