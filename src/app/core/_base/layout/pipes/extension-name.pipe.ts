// Angular
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Returns size name from bytes
 */
@Pipe({
	name: 'extensionNameIcon'
})
export class ExtensionNameIconPipe implements PipeTransform {

	/**
	 * Transform
	 *
	 * @param value: any
	 * @param args: any
	 */
	transform(value: string, args?: any): any {
		let baseUrl: string = 'assets/media/extensions/';
		let val = value.match(/\.[0-9a-z]+$/i)[0];
		val = val.substring(1);
		val = val.toLowerCase();

		return baseUrl + val + '.svg';
    }
}
