// Angular
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Returns size name from bytes
 */
@Pipe({
	name: 'formatSize'
})
export class FormatSizePipe implements PipeTransform {

	/**
	 * Transform
	 *
	 * @param value: any
	 * @param args: any
	 */
	transform(bytes: number, decimals: number = 1, returnType: null|'value'|'name' = null): any {
        if(bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals <= 0 ? 0 : decimals || 1,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));

		if(returnType == null) {
			return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
		} else if (returnType == 'name'){
			return sizes[i];
		} else if (returnType == 'value') {
			return parseFloat((bytes / Math.pow(k, i)).toFixed(dm))
		}
    }
}
