// France
export const locale = {
	lang: 'fr',
	data: {
		COMMON: {
			OR: 'ou'
		},
		MENU: {
			OVERVIEW: `Vue d'ensemble`,
			REQUESTS: 'Demandes',
			SUBMENU: {
				NEW_REQUEST: 'Nouvelle demande',
				DELIVERED: 'Livré',
				TO_APPROVAL: 'Pour approbation',
				IN_DEVELOPMENT: 'En développement',
				SOCIAL_MEDIA: 'Médias sociaux',
				STRATEGY: 'Stratégie',
				DEVELOPMENT: 'Développement',
				CUSTOM: 'Personnalisé',
				ALL: 'Tous',
			}
		},
		AUTH: {
			WELCOME: 'Bienvenue sur _mybrief',
			WELCOME_DESCRIPTION: 'Bienvenue sur _mybrief description',
			REGISTER: {
				TITLE: `S'inscrire`
			},
			SIGN_IN: {
				TITLE: 'Se connecter'
			},
			INPUT: {
				EMAIL: 'Email',
				USERNAME_OR_EMAIL: `Nom d'utilisateur ou Email`,
				PASSWORD: 'Mot de passe',
				CONFIRM_PASSWORD: 'Confirmation mot de passe',
				FULLNAME: 'Nom complet',
				USERNAME: `Nom d'utilisateur`
			},
			GENERAL: {
				FORGOT_BUTTON: 'Mot de passe oublié',
				LOGIN_BUTTON: 'Se connecter',
				SIGNUP_BUTTON: 'Créer un compte',
				BACK_BUTTON: 'Retour à la connexion',
				SUBMIT_BUTTON: 'Créer un compte',
				AGREE_TERMS_AND_CONDITION: `J'accepte les <a href="#">termes et conditions</a>`,
				DONT_HAVE_AN_ACCOUNT_YET: `Vous n'avez pas encore de compte?`
			},
			VALIDATION: {
				REQUIRED_FIELD: 'Champ obligatoire',
				MIN_LENGTH_FIELD: 'Min caractères'
			}
		}
	}
};
