// USA
export const locale = {
	lang: 'pt',
	data: {
		COMMON: {
			OR: 'ou'
		},
		MENU: {
			OVERVIEW: 'Visão Geral',
			REQUESTS: 'Solicitações',
			SUBMENU: {
				NEW_REQUEST: 'Nova requisição',
				DELIVERED: 'Entregues',
				TO_APPROVAL: 'Para aprovação',
				IN_DEVELOPMENT: 'Em desenvolvimento',
				SOCIAL_MEDIA: 'Mídias sociais',
				STRATEGY: 'Estratégia',
				DEVELOPMENT: 'Desenvolvimento',
				CUSTOM: 'Customizado',
				ALL: 'Todas',
			}
		},
		AUTH: {
			WELCOME: 'Seja bem vinda(o) a _mybrief',
			WELCOME_DESCRIPTION: 'Seja bem vinda(o) a _mybrief descrição',
			REGISTER: {
				TITLE: 'Registrar-se'
			},
			SIGN_IN: {
				TITLE: 'Entrar'
			},
			INPUT: {
				EMAIL: 'Email',
				USERNAME_OR_EMAIL: 'Usuário ou Email',
				PASSWORD: 'Senha',
				CONFIRM_PASSWORD: 'Confirmação de senha',
				FULLNAME: 'Nome completo',
				USERNAME: 'Nome de usuário'
			},
			GENERAL: {
				FORGOT_BUTTON: 'Esqueceu a senha',
				LOGIN_BUTTON: 'Entrar',
				SIGNUP_BUTTON: 'Criar uma conta',
				BACK_BUTTON: 'Voltar para login',
				SUBMIT_BUTTON: 'Criar a conta',
				AGREE_TERMS_AND_CONDITION: 'Eu concordo com os <a href="#">termos e condições</a>',
				DONT_HAVE_AN_ACCOUNT_YET: `Não tem uma conta ainda?`
			},
			VALIDATION: {
				REQUIRED_FIELD: 'Campo obrigatório',
				MIN_LENGTH_FIELD: 'Caracteres mínimo'
			}
		}
	}
};
