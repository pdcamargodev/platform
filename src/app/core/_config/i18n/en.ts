// USA
export const locale = {
	lang: 'en',
	data: {
		COMMON: {
			OR: 'or'
		},
		MENU: {
			OVERVIEW: 'Overview',
			REQUESTS: 'Requests',
			SUBMENU: {
				NEW_REQUEST: 'New request',
				DELIVERED: 'Delivered',
				TO_APPROVAL: 'To approval',
				IN_DEVELOPMENT: 'In development',
				SOCIAL_MEDIA: 'Social media',
				STRATEGY: 'Strategy',
				DEVELOPMENT: 'Development',
				CUSTOM: 'Custom',
				ALL: 'All',
			}
		},
		AUTH: {
			WELCOME: 'Welcome to _mybrief',
			WELCOME_DESCRIPTION: 'Welcome to _mybrief description',
			REGISTER: {
				TITLE: 'Sign up'
			},
			SIGN_IN: {
				TITLE: 'Sign in'
			},
			INPUT: {
				EMAIL: 'Email',
				USERNAME_OR_EMAIL: 'Username or Email',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Password confirmation',
				FULLNAME: 'Fullname',
				USERNAME: 'Username',
			},
			GENERAL: {
				FORGOT_BUTTON: 'Forgot password',
				LOGIN_BUTTON: 'Sign in',
				SIGNUP_BUTTON: 'Create an account',
				BACK_BUTTON: 'Back to login',
				SUBMIT_BUTTON: 'Create Account',
				AGREE_TERMS_AND_CONDITION: 'I agree the <a href="#">terms and conditions</a>',
				DONT_HAVE_AN_ACCOUNT_YET: `Don't have an account yet?`
			},
			VALIDATION: {
				REQUIRED_FIELD: 'Required field',
				MIN_LENGTH_FIELD: 'Min characters'
			}
		}
	}
};
