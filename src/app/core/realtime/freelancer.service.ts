import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, Observer } from 'rxjs';
import { JobCommentary } from '../_base/crud/models/job';
import { AuthService, CurrentUser } from '../_services';
import { Freelancer } from '../_base/crud/models/freelancer';

@Injectable({
  providedIn: 'root'
})
export class FreelancerService {

  constructor(private socket: Socket, private authService: AuthService) {
    this.connect();
  }

  /**
   |
   | Methods to login to freelancers channel, logout/disconnect
   |
   */

  /**
   * ### Method to disconnect freelancers from channel / socket
   *
   * It is only called `socket.disconnect` because socket.io handles all channels disconnection as well when is called disconnect method
   */
  disconnect(): void {
    this.socket.disconnect();
  }

  /**
   * ### Method to connect freelancer to freelancers channel on server
   * The system will validate if current user is a freelancer and load his base information from cache and send the ID to server
   */
  connect(): void {
    let currentUser: CurrentUser = this.authService.getCurrentUser();

    if(currentUser != null && currentUser.userType == 'freelancer') {
      let freelancer: Freelancer = currentUser.data;
      this.socket.emit('connectFreelancer', {
        freelancerId: freelancer.id,
        username: freelancer.username
      });
    }
  }


  /**
   |
   | Listeners to be used with freelancer methods
   |
   */

  /**
   * ### Observable to return when admin comments in a freelancer job
   *
   * It will return a formatted commentary with `JobCommentary` interface
   *
   * @returns ``JobCommentary``
   */
  onAdminCommentInJob(): Observable<JobCommentary> {
    return Observable.create((observer: Observer<JobCommentary>) => {
      this.socket.on('onAdminCommentingInJob', (data: JobCommentary ) => {
        observer.next(data);
      });
    });
  }
}
