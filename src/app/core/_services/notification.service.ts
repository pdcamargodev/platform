import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

type Notification = {
	id: string
	title: string
	description?: string
	go_to?: string
	created_at: Date
}

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
	observer = new Subject<Notification[]>();

	public notifications = this.observer.asObservable();

	public sendNotifications(notifications: Notification[]): void {
		this.observer.next(notifications);
	}
}
