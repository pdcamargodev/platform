import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Product, Plan, Package } from './../_base/crud/models';

import { editArrayByIndex, arrayHasIndex } from '../_utils';

export type CartItem  = {
	type: 'product'|'plan'|'package'
	quantity: number
	item: Product | Plan| Package
}

@Injectable({
  providedIn: 'root'
})
export class CartService {
	public get cartItemKey() {
		return "_mb2#20#c@i";
	}

	private observer = new Subject<CartItem[]>();

	public items = this.observer.asObservable();

	public addCartItems(items: CartItem[]): CartItem[] {
		const cart = this.storeItems(items);

		return cart;
	}

	public getCartItems() {
		const storedItems = localStorage.getItem(this.cartItemKey);

		return !storedItems || storedItems.trim() === '' ? [] : JSON.parse(storedItems) as CartItem[];
	}

	private setCart(cart: CartItem[]): CartItem[] {
		localStorage.setItem(this.cartItemKey, JSON.stringify(cart));

		return cart;
	}

	private notifySubscribers(cart: CartItem[]): void {
		this.observer.next(cart);
	}

	private storeItems(items: CartItem[] = []): CartItem[] {
		const currentItems = this.getCartItems();

		const newCart = [...items, ...currentItems];

		this.setCart(newCart)

		this.notifySubscribers(newCart);

		return newCart;
	}

	public removeItem(index: number): CartItem[] {
		const items = this.getCartItems();

		const filteredItems = items.filter((_item, itemIndex) => itemIndex !== index);

		this.setCart(filteredItems);

		this.notifySubscribers(filteredItems);

		return filteredItems;
	}

	public setItemQuantity(index: number, quantity: number): CartItem[] {
		let items = this.getCartItems();

		const item = items.find((_item, cartIndex) => cartIndex === index);

		item.quantity = quantity;

		items = editArrayByIndex(items, index, item);

		this.setCart(items)

		this.notifySubscribers(items);

		return items;
	}

	public getCartTotalPrice(): {cash: number, coin: number} {
		const items = this.getCartItems();

		return {
			cash: items.reduce((a, b) => a + b.item.price * b.quantity, 0),
			coin: items.reduce((a, b) => a + (b.type === 'plan' ? 0 : b.item['price_coin'] * b.quantity), 0)
		}
	}
}
