import { Injectable } from '@angular/core';
import { Freelancer } from '../_base/crud/models/freelancer';
import { CompanyUser } from '../_base/crud/models/company';
import { Admin } from '../_base/crud/models/admin';

import CryptoJS from 'crypto-js';

export interface CurrentUser {
  userType: 'admin'|'company'|'freelancer';
  data: Freelancer|CompanyUser|Admin;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() {}

  public get userKey(): string {
    return '_mb2#19@';
  }

  public get tokenKey(): string {
    return '_mb2#19#t@a';
  }

  getCurrentUser(): CurrentUser|null {
    let currentUser: CurrentUser = this.unhashUser(localStorage.getItem(this.userKey));
    if(this.getCurrentUserToken() == null || currentUser == null) {
      currentUser = null;
      this.logout();
    }

    return currentUser;
  }

  getCurrentUserToken(): string|null {
    let t: string|null = localStorage.getItem(this.tokenKey);
    if(t == null || t.trim() == '') {
      t = null;
    }
    return t;
  }

  setCurrentUser(f: Freelancer|CompanyUser|Admin, at: string, ut: 'admin'|'freelancer'|'company'): void {
    let currentUser: CurrentUser = {
      userType: ut,
      data: f
    }
    localStorage.setItem(this.userKey, this.hashUser(currentUser));
    localStorage.setItem(this.tokenKey, at);
  }

  logout(): void {
    localStorage.removeItem(this.userKey);
    localStorage.removeItem(this.tokenKey);
  }

  private hashUser(a: CurrentUser): string {
    let hashedCurrentUser: string = JSON.stringify(a);
    return CryptoJS.AES.encrypt(hashedCurrentUser, this.userKey).toString();
  }

  private unhashUser(b: any): CurrentUser|null {
    if(b == null) {
      return null
    }

    let d: string = CryptoJS.AES.decrypt(b.toString(), this.userKey).toString(CryptoJS.enc.Utf8);

    if(d == '') {
      return null;
    }

    return JSON.parse(d);
  }
}
