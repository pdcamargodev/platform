// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
import { ErrorPageComponent } from './views/theme/content/error-page/error-page.component';
import { AuthGuard, IsAdminAuthGuard, IsFreelancerAuthGuard, IsCompanyAuthGuard } from './core';
import { JobsComponent } from './views/pages/freelancer/jobs/jobs.component';
import { JobsComponent as AdminJobsComponent} from './views/pages/admin/jobs/jobs.component';

//TODO: AuthGuard

const routes: Routes = [
	{
		path: 'prestador/tarefas',
		component: JobsComponent,
		canActivate: [AuthGuard, IsFreelancerAuthGuard]
	},
	{
		path: 'administrador/tarefas',
		component: AdminJobsComponent,
		canActivate: [IsAdminAuthGuard]
	},
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'prestador',
				loadChildren: () => import('./views/pages/freelancer/freelancer.module').then(m => m.FreelancerModule),
				canActivate: [IsFreelancerAuthGuard]
			},
			{
				path: 'empresa',
				loadChildren: () => import('./views/pages/company/company.module').then(m => m.CompanyModule),
				canActivate: [IsCompanyAuthGuard]
			},
			{
				path: 'administrador',
				loadChildren: () => import('./views/pages/admin/admin.module').then(m => m.AdminModule),
				canActivate: [IsAdminAuthGuard]
			},
			{
				path: 'builder',
				loadChildren: () => import('./views/theme/content/builder/builder.module').then(m => m.BuilderModule)
			},
			{
				path: '404',
				component: ErrorPageComponent,
				data: {
					'type': 'error-v3',
					'code': 404,
					'title': '404... Não encontrado',
					'desc': 'Não encontramos o conteúdo que você está buscando'
				}
			}
		]
	},
	{
		path: 'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule)
	},
	{path: '**', redirectTo: '404', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
