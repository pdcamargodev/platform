import { Subscription } from 'rxjs';
// Angular
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router, ChildActivationEnd } from '@angular/router';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService } from './core/_base/layout';
// language list
import { locale as enLang } from './core/_config/i18n/en';
import { locale as frLang } from './core/_config/i18n/fr';
import { locale as ptBrLang } from './core/_config/i18n/pt-BR';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'body[kt-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
	// Public properties
	title = 'Metronic';
	loader: boolean;
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 * @param layoutConfigService: LayoutCongifService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(private translationService: TranslationService,
				private router: Router,
				private layoutConfigService: LayoutConfigService,
				private splashScreenService: SplashScreenService) {

		// register translations
		this.translationService.loadTranslations(enLang, frLang, ptBrLang);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		// enable/disable loader
		this.loader = this.layoutConfigService.getConfig('loader.enabled');

		const routerSubscription = this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				// hide splash screen
				this.splashScreenService.hide();

				// scroll to top on every route change
				window.scrollTo(0, 0);

				// to display back the body content
				setTimeout(() => {
					document.body.classList.add('kt-page--loaded');
				}, 500);
			}
		});
		this.unsubscribe.push(routerSubscription);

		document.addEventListener('click', function(e) {
			// Array.prototype.forEach.call(document.querySelectorAll('.dropdown-menu'), function(element) {
			// 	element.classList.remove('show');
			// });

			// loop parent nodes from the target to the delegation node
			for (var target: any = e.target; target && target != this; target = target['parentNode']) {
				if(target.getAttribute('href') != null && target.getAttribute('href') != '' && target.getAttribute('href').charAt(0) == '#') {
					e.preventDefault()
				}

				if (target.matches('.alert-close')) {
					let parent = target.closest('[role="alert"]');

					parent.parentNode.removeChild(parent);
					break;
				}

				if(target.matches('[data-toggle="dropdown"]')) {
					Array.prototype.forEach.call(target.closest('.dropdown').children, function(child){
						if(child !== target && child.classList.contains('dropdown-menu')) {
							child.classList.toggle('show');
							return;
						}
					});
					break;
				}

				if(target.matches('[data-toggle="tab"]')) {
					e.preventDefault();
					let li: HTMLElement = target.parentNode;
					let tabId = target.getAttribute('href');
					let tab = document.getElementById(tabId.substr(1));

					if (tab != null) {
						Array.prototype.forEach.call(tab.parentNode.children, function(child: HTMLElement){
							if(child === tab) {
								tab.classList.add('active');
							} else {
								child.classList.remove('active');
							}
						});

						Array.prototype.forEach.call(li.parentNode.children, function(child: HTMLElement){
							if(child.children.length > 0) {
								if(child === li) {
									child.children[0].classList.add('active');
								} else {
									child.children[0].classList.remove('active');
								}
							}
						});

						break;
					}
				}
			}
		}, false);
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}
}
