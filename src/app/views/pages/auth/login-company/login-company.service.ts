import { Injectable } from '@angular/core';
import { CompanyLoginResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginCompanyService {

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<CompanyLoginResponse> {
    return this.http.post<CompanyLoginResponse>(environment.routes.auth.login.company, {
      username: username,
      password: password
    });
  }
}
