import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../../core/_services';
import { MatSnackBar } from '@angular/material';
import { CompanyLoginResponse } from '../../../../core/_base/crud/models/responses';
import { CompanyUser } from '../../../../core/_base/crud/models/company';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginCompanyService } from './login-company.service';

@Component({
  selector: 'kt-login-company',
  templateUrl: './login-company.component.html',
  styleUrls: ['./login-company.component.scss']
})
export class LoginCompanyComponent implements OnInit, OnDestroy {

  // Public params
	loginForm: FormGroup;
	loading = false;
	errors: any = [];

	private unsubscribe: Subscription[] = [];

  private returnUrl: any;

  constructor(
		private router: Router,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private authService: AuthService,
		private loginService: LoginCompanyService,
		private snackBar: MatSnackBar,
		private cdr: ChangeDetectorRef
	) {
		if(authService.getCurrentUser() != null) {
			this.router.navigateByUrl('/dashboard');
			return;
		}
	}

  ngOnInit() {
    this.initLoginForm();

		// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'] || '/';
		});
  }

  /**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.forEach(subs => subs.unsubscribe());
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		this.loginForm = this.fb.group({
			username: ['', Validators.compose([
        Validators.required,
        Validators.email,
				Validators.minLength(3),
				Validators.maxLength(255) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])],
			password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(255)
			])]
		});
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	submit() {
		if(this.loading) {
			return
		}

		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			username: controls['username'].value,
			password: controls['password'].value
		};

		let subs: Subscription = this.loginService.login(authData.username, authData.password)
			.subscribe((obj: CompanyLoginResponse) => {
				this.loading = false;
				this.cdr.markForCheck();

				if(!obj.error) {
					let user: CompanyUser = obj.data.company_user;
					let accessToken: string = obj.data.access_token;
					this.authService.setCurrentUser(user, accessToken, 'company');
					this.router.navigateByUrl('/empresa');
				}
			}, (error: HttpErrorResponse) => {
				if(error.status == 401 && error.error != null && error.error.statusCode == 401) {
					this.snackBar.open('Usuário ou senha inválida', 'Entendi', {
						duration: 3000
					});
				}

				this.loading = false;
				this.cdr.markForCheck();
			});

		this.unsubscribe.push(subs);
	}
}
