// Angular
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// Material
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';
// Translate
import { TranslateModule } from '@ngx-translate/core';
// CRUD
import { InterceptService } from '../../../core/_base/crud/';
// Module components
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthGuard, IsAdminAuthGuard } from '../../../core/_auth';
import { AuthService } from '../../../core/_services';

import { LoginAdminComponent } from './login-admin/login-admin.component';
import { LoginCompanyComponent } from './login-company/login-company.component';
import { RegisterCompanyComponent } from './register-company/register-company.component';

const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [
			{
				path: 'prestador/entrar',
				redirectTo: '/entrar',
				pathMatch: 'full'
			},
			{
				path: 'entrar',
				component: LoginComponent,
				data: {returnUrl: window.location.pathname}
			},
			{
				path: 'empresa/entrar',
				component: LoginCompanyComponent,
				data: {returnUrl: window.location.pathname}
			},
			{
				path: 'admin/entrar',
				component: LoginAdminComponent,
				data: {returnUrl: window.location.pathname}
			},
			{
				path: 'cadastro',
				component: RegisterComponent
			},
			{
				path: 'empresa/cadastro',
				component: RegisterCompanyComponent
			},
			{
				path: 'esqueci-minha-senha',
				component: ForgotPasswordComponent,
			}
		]
	}
];


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatButtonModule,
		RouterModule.forChild(routes),
		MatInputModule,
		MatFormFieldModule,
		MatCheckboxModule,
		TranslateModule.forChild(),
	],
	providers: [
		InterceptService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
	],
	exports: [AuthComponent],
	declarations: [
		AuthComponent,
		LoginComponent,
		RegisterComponent,
		RegisterCompanyComponent,
		ForgotPasswordComponent,
		LoginAdminComponent,
		LoginCompanyComponent,
	]
})

export class AuthModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: AuthModule,
			providers: [
				AuthGuard,
				IsAdminAuthGuard,
				AuthService
			]
		};
	}
}
