import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdminLoginResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginAdminService {

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<AdminLoginResponse> {
    return this.http.post<AdminLoginResponse>(environment.routes.auth.login.admin, {
      username: username,
      password: password
    });
  }
}
