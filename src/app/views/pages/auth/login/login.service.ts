import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FreelancerLoginResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from './../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<FreelancerLoginResponse> {
    return this.http.post<FreelancerLoginResponse>(environment.routes.auth.login.freelancer, {
      username: username,
      password: password
    });
  }
}
