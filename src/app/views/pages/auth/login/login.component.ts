// Angular
import { Component, OnDestroy, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { Subject } from 'rxjs';
// Store
import { AuthService } from '../../../../core/_services';
import { Freelancer } from '../../../../core/_base/crud/models/freelancer/freelancer.model';
import { LoginService } from './login.service';
import { FreelancerLoginResponse } from '../../../../core/_base/crud/models/responses';
import { HttpErrorResponse } from '@angular/common/http';

import { MatSnackBar } from '@angular/material';

@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {
	// Public params
	loginForm: FormGroup;
	loading = false;
	errors: any = [];

	private unsubscribe: Subject<any>;

	private returnUrl: any;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
	constructor(
		private router: Router,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private authService: AuthService,
		private loginService: LoginService,
		private snackBar: MatSnackBar,
		private cdr: ChangeDetectorRef
	) {
		if(authService.getCurrentUser() != null) {
			this.router.navigateByUrl('/');
			return;
		}

		this.unsubscribe = new Subject();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.initLoginForm();

		// redirect back to the returnUrl before login
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'] || '/';
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		if(this.unsubscribe != null) {
			this.unsubscribe.next();
			this.unsubscribe.complete();
		}

		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		this.loginForm = this.fb.group({
			emailOrUsername: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(255) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			])
			],
			password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(255)
			])
			]
		});
	}

	/**
	 * Form Submit
	 */
	submit() {
		if(this.loading) {
			return
		}

		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			username: controls['emailOrUsername'].value,
			password: controls['password'].value
		};

		this.loginService.login(authData.username, authData.password)
			.subscribe((obj: FreelancerLoginResponse) => {
				this.loading = false;
				this.cdr.markForCheck();

				if(!obj.error) {
					let freelancer: Freelancer = obj.data.freelancer;
					let accessToken: string = obj.data.access_token;
					this.authService.setCurrentUser(freelancer, accessToken, 'freelancer');
					this.router.navigateByUrl('/prestador');
				}
			}, (error: HttpErrorResponse) => {
				if(error.status == 401 && error.error != null && error.error.statusCode == 401) {
					this.snackBar.open('Usuário ou senha inválida', 'Entendi', {
						duration: 3000
					});
				}

				this.loading = false;
				this.cdr.markForCheck();
			});
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
