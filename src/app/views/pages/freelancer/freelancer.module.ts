// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Angular Material
import { MatTooltipModule, MatSnackBarModule, MatAutocompleteModule, MatFormFieldModule, MatInputModule } from '@angular/material';
// Core Modules
import { ProfileComponent } from './profile/profile.component';
import { FinancialComponent } from './financial/financial.component';
import { OverviewComponent } from './profile/overview/overview.component';
import { PersonalInformationComponent } from './profile/personal-information/personal-information.component';
import { FinancialInformationComponent } from './profile/financial-information/financial-information.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { AccountConfigurationComponent } from './profile/account-configuration/account-configuration.component';
import { PartialsModule } from '../../partials/partials.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    ProfileComponent,
    FinancialComponent,
    OverviewComponent,
    PersonalInformationComponent,
    FinancialInformationComponent,
    ChangePasswordComponent,
    AccountConfigurationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    NgbPopoverModule,
    PartialsModule,
    RouterModule.forChild([
      {
        path: 'financeiro',
        component: FinancialComponent
      },
      {
				path: '',
        redirectTo: '/prestador/perfil/geral',
        pathMatch: 'full'
      },
      {
        path: 'perfil',
        redirectTo: '/prestador/perfil/geral',
        pathMatch: 'full'
      },
      {
        path: 'perfil/:page',
        component: ProfileComponent
      },
		])
  ]
})
export class FreelancerModule { }
