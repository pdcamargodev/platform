import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'kt-profile-account-configuration',
  templateUrl: './account-configuration.component.html',
  styleUrls: ['./account-configuration.component.scss']
})
export class AccountConfigurationComponent implements OnInit {

  constructor(private titleService: Title) {
    titleService.setTitle('Configurações de conta | _mybrief');
  }

  ngOnInit() {
  }

}
