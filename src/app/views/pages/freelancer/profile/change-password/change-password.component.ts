// Angular
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
// RXJS
import { Subscription } from 'rxjs';
// Core
import { ChangePasswordService } from './change-password.service';
import { MatchValues } from '../../../../../../app/core/validators';
import { BaseResponse } from '../../../../../../app/core/_base/crud/models/responses';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'kt-profile-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  // Public params
  changePasswordForm: FormGroup;
  loading: boolean = false;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    private titleService: Title,
    private changePasswordService: ChangePasswordService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    titleService.setTitle('Mudar senha | _mybrief');
  }

  ngOnInit() {
    this.initLoginForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  /**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		this.changePasswordForm = this.fb.group({
			oldPassword: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(255)
        ])
			],
			newPassword: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(255)
			  ])
			],
			confirmNewPassword: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(255),
          MatchValues.matchValues('newPassword'),
			  ])
			]
		});
	}

  submit() {
    if(this.loading) {
      return
    }

    const controls = this.changePasswordForm.controls;
		/** check form */
		if (this.changePasswordForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    this.loading = true;

    const payload = {
			old_password: controls['oldPassword'].value,
			new_password: controls['newPassword'].value
    };

    let subs = this.changePasswordService.changePassword(payload.old_password, payload.new_password)
      .subscribe((obj: BaseResponse) => {
        this.loading = false;
        this.cdr.markForCheck();

        this.snackBar.open(obj.message, 'Entendi', {
          duration: 3000
        });
      }, (error: HttpErrorResponse) => {
        if(error.status == 401 && error.error != null && error.error.statusCode == 401) {
          this.snackBar.open('Usuário ou senha inválida', 'Entendi', {
            duration: 3000
          });
        }

        this.loading = false;
        this.cdr.markForCheck();
      });

    this.subscriptions.push(subs);
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.changePasswordForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
