import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  constructor(private http: HttpClient) {}

  changePassword(oldPassword: string, newPassword: string): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.freelancer.password.change, {
      old_password: oldPassword,
      new_password: newPassword
    });
  }
}
