// Core
import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
// Core
import { Freelancer } from '../../../../../core/_base/crud/models/freelancer';
import { FinancialInformationService } from './financial-information.service';
// Others
import moment from 'moment';
import Swal from 'sweetalert2';
import { BaseResponse } from '../../../../../core/_base/crud/models/responses';
import { HttpErrorResponse } from '@angular/common/http';
import { startWith, map } from 'rxjs/operators';

interface Bank {
  id?: number;
  name?: string;
}

@Component({
  selector: 'kt-profile-financial-information',
  templateUrl: './financial-information.component.html',
  styleUrls: ['./financial-information.component.scss']
})
export class FinancialInformationComponent implements OnInit, OnDestroy {
  @Input('freelancer') freelancer: Freelancer;

  moment = moment

  // Public params
  changeTimeAvailable: FormGroup;
  updateBankAccountAvailable: FormGroup;
  loading: boolean = false;
  loadingSavingBank: boolean = false;

  loadingBanks: boolean = false
  bankList: any[] = [];

  // Private params
  private subscriptions: Subscription[] = [];

  banksControl: FormControl = new FormControl();
  filteredBanks: Observable<any[]>;

  constructor(
    private titleService: Title,
    private financialInformationService: FinancialInformationService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    titleService.setTitle('Informações financeiras | _mybrief');
  }

  ngOnInit() {
    this.initTimeAvailableForm();
    this.initBankAccountForm();
    // this.getBankList();

    this.filteredBanks = this.banksControl.valueChanges
      .pipe(
        startWith<string | Bank>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this.filter(name) : this.bankList.slice())
      );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  initBankAccountForm() {
    this.updateBankAccountAvailable = this.fb.group({
      bank: [
        this.freelancer.bank_account.bank,
        Validators.compose([
          Validators.required
        ])
      ],
      agency: [
        this.freelancer.bank_account.agency,
        Validators.compose([
          Validators.required
        ])
      ],
      account: [
        this.freelancer.bank_account.account,
        Validators.compose([
          Validators.required
        ])
      ],
      full_name: [
        this.freelancer.bank_account.account,
        Validators.compose([
          Validators.required
        ])
      ],
      cpf: [
        this.freelancer.bank_account.account,
        Validators.compose([
          Validators.required
        ])
      ]
    })
  }

  initTimeAvailableForm() {
		this.changeTimeAvailable = this.fb.group({
      monday: [
        {
          value: this.freelancer.time_available.monday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      saturday: [
        {
          value: this.freelancer.time_available.saturday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      sunday: [
        {
          value: this.freelancer.time_available.sunday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      thursday: [
        {
          value: this.freelancer.time_available.thursday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      tuesday: [
        {
          value: this.freelancer.time_available.tuesday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      wednesday: [
        {
          value: this.freelancer.time_available.wednesday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
      ],
      friday: [
        {
          value: this.freelancer.time_available.friday,
          disabled: !this.freelancer.time_available.can_update_time_available
        },
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(8)
        ])
			],
		});
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.changeTimeAvailable.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

  getTotalAvailableHours(): number {
    let hour: number = 0;

    hour += this.freelancer.time_available.friday;
    hour += this.freelancer.time_available.monday;
    hour += this.freelancer.time_available.saturday;
    hour += this.freelancer.time_available.sunday;
    hour += this.freelancer.time_available.thursday;
    hour += this.freelancer.time_available.tuesday;
    hour += this.freelancer.time_available.wednesday;

    return hour;
  }

  requestNewAnalysis(): void {
    Swal.fire({
      icon: 'question',
      title: 'Solicitar reanálise?',
      reverseButtons: true,
      showCancelButton: true,
      confirmButtonText: 'Pedir reanálise',
      cancelButtonText: 'Cancelar reanálise',
      buttonsStyling: false,
      html: `
        <div class="text-left">
          <p>Nós da _mybrief priorizamos o bem-estar de todos da comunidade e por esse motivo pedimos que qualquer alteração no fluxo de trabalho seja através do dialogo e bom senso. Por isso, nos informe o motivo da reanálise no campo abaixo e clique em <b>pedir reanálise</b>.</p>
        </div>
      `,
      input: 'textarea',
      inputValidator: result => !result && 'Por favor, nos informe o motivo!',
      customClass: {
        title: 'h3 font-weight-bold',
        input: 'form-control',
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-secondary',
      }
    }).then((result) => {
      if(result.value) {
        let reason = result.value;

        this.sendRequestToAnalysis(reason);
      }
    });
  }

  async sendRequestToAnalysis(reason: string) {
    let obj = await this.financialInformationService.requestReanalysis(reason).toPromise();

    this.snackBar.open(obj.message, 'Entendi', {
      duration: 3000
    });

    if(!obj.error) {
      this.freelancer.time_available.requested_analysis_at = moment().toDate();
      this.cdr.markForCheck();
    }
  }

  updateBankAccount(): void {
    if(this.loadingSavingBank) {
      return
    }

    const controls = this.updateBankAccountAvailable.controls;
		/** check form */
		if (this.updateBankAccountAvailable.invalid) {
			Object.keys(controls).forEach(controlName => {
				console.log(controlName)
				controls[controlName].markAsTouched() }
			);
			return;
    }

    this.loadingSavingBank = true;

    const payload = {
			bank: controls['bank'].value,
			agency: controls['agency'].value,
			account: controls['account'].value,
			full_name: controls['full_name'].value,
			cpf: controls['cpf'].value,
			// account_type: controls['account_type'].value,
			account_type: 'checking'
    };

    let subs = this.financialInformationService.updateBankAccount(payload)
      .subscribe((obj: BaseResponse) => {
        this.loadingSavingBank = false;

        this.snackBar.open(obj.message, 'Entendi', {
          duration: 3000
        });

        if(!obj.error) {
          this.freelancer.bank_account.bank = payload.bank;
          this.freelancer.bank_account.account = payload.account;
          this.freelancer.bank_account.agency = payload.agency;
          this.freelancer.bank_account.full_name = payload.full_name;
          this.freelancer.bank_account.cpf = payload.cpf;
        }

        this.cdr.markForCheck();
      }, (error: HttpErrorResponse) => {
        if(error.status == 401 && error.error != null && error.error.message != null) {
          this.snackBar.open(error.error.message, 'Entendi', {
            duration: 3000
          });
        }

        this.loadingSavingBank = false;
        this.cdr.markForCheck();
      });

    this.subscriptions.push(subs);
  }

  updateTimeAvailable(): void {
    if(this.loading) {
      return
    }

    const controls = this.changeTimeAvailable.controls;
		/** check form */
		if (this.changeTimeAvailable.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    this.loading = true;

    const payload = {
			friday: controls['friday'].value,
			monday: controls['monday'].value,
			saturday: controls['saturday'].value,
			sunday: controls['sunday'].value,
			thursday: controls['thursday'].value,
			tuesday: controls['tuesday'].value,
			wednesday: controls['wednesday'].value
    };

    let subs = this.financialInformationService.updateTimeAvailable(payload)
      .subscribe((obj: BaseResponse) => {
        this.loading = false;
        this.cdr.markForCheck();

        this.snackBar.open(obj.message, 'Entendi', {
          duration: 3000
        });

        if(!obj.error) {
          this.freelancer.time_available.can_update_time_available = 0;

          this.freelancer.time_available.friday = payload.friday;
          this.freelancer.time_available.monday = payload.monday;
          this.freelancer.time_available.saturday = payload.saturday;
          this.freelancer.time_available.sunday = payload.sunday;
          this.freelancer.time_available.thursday = payload.thursday;
          this.freelancer.time_available.tuesday = payload.tuesday;
          this.freelancer.time_available.wednesday = payload.wednesday;
        }

        this.cdr.markForCheck();
      }, (error: HttpErrorResponse) => {
        if(error.status == 401 && error.error != null && error.error.message != null) {
          this.snackBar.open(error.error.message, 'Entendi', {
            duration: 3000
          });
        }

        this.loading = false;
        this.cdr.markForCheck();
      });

    this.subscriptions.push(subs);
  }

  async getBankList() {
    this.loadingBanks = true;
    this.bankList = await this.financialInformationService.getBankList().toPromise();

    this.loadingBanks = false;
    this.cdr.markForCheck();
  }

  filter(name: string): any[] {
    return this.bankList.filter(option =>
      option.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  displayFn(bank: Bank): string | undefined {
    return bank ? bank.name : undefined;
  }
}
