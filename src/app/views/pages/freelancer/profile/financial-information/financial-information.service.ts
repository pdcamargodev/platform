import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseResponse } from '../../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FinancialInformationService {

  constructor(private http: HttpClient) { }

  public requestReanalysis(reason: string): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(environment.routes.freelancer.financial.time_available.reanalysis, {
        description: reason
    })
  }

  public updateTimeAvailable(payload: object): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.freelancer.financial.time_available.update, payload);
  }

  public updateBankAccount(payload: object): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.freelancer.financial.bank_account.update, payload);
  }

  public getBankList(): Observable<any[]> {
    return this.http.get<any[]>(environment.routes.common.bank.list);
  }
}
