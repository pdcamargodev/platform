import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { OverviewService } from './overview.service';
import { Subscription } from 'rxjs';
import { GetFreelancerNotificationResponse, GetFreelancerJobsResponse } from '../../../../../core/_base/crud/models/responses';
import { FreelancerNotification, Freelancer } from '../../../../../core/_base/crud/models/freelancer';
import { HttpErrorResponse } from '@angular/common/http';

import moment from 'moment';
import { Router } from '@angular/router';
import { Job } from '../../../../../core/_base/crud/models/job';

@Component({
  selector: 'kt-profile-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {
  @Input('freelancer') freelancer: Freelancer;

  moment = moment

  private subscriptions: Subscription[] = [];

  notifications: FreelancerNotification[] = [];
  latestJobs: Job[] = [];

  loadingNotifications: boolean = false;
  loadingLatestJobs: boolean = false;

  constructor(
    private titleService: Title,
    private overviewService: OverviewService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) {
    titleService.setTitle('Visão geral | _mybrief');
  }

  ngOnInit(): void {
    this.getNotifications();
    this.getLatestJobs();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  getLatestJobs(): void {
    this.loadingLatestJobs = true;

    let subs = this.overviewService.getLatestJobs()
      .subscribe((obj: GetFreelancerJobsResponse) => {
        this.loadingLatestJobs = false;

        this.latestJobs = obj.data;
        this.cdr.markForCheck();
      }, (error: HttpErrorResponse) => {
        this.loadingLatestJobs = false;
      });

    this.subscriptions.push(subs);
  }

  getNotifications(): void {
    this.loadingNotifications = true;
    let subs = this.overviewService.getNotifications()
      .subscribe((obj: GetFreelancerNotificationResponse) => {
        this.loadingNotifications = false;
        if(!obj.error) {
          this.notifications = obj.data;
          this.cdr.markForCheck();
        }
      }, (error: HttpErrorResponse) => {
        this.loadingNotifications = false;
      });

    this.subscriptions.push(subs);
  }

  statusTooltip(status: string): string {
    if(status == 'doing') {
      return 'fazendo'
    } else if (status == 'to_do') {
      return 'para fazer'
    } else if(status == 'to_approval') {
      return 'para aprovação'
    } else if(status == 'to_change') {
      return 'para alteração'
    } else if(status == 'done') {
      return 'concluído'
    }
  }

  onNotificationClick(event: Event, goTo: string|null): void {
    event.preventDefault();
    if(goTo != null) {
      this.router.navigateByUrl(goTo);
    }
  }
}
