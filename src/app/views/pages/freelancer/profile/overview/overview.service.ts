import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetFreelancerNotificationResponse, GetFreelancerJobsResponse } from '../../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OverviewService {

  constructor(private http: HttpClient) { }

  public getNotifications(): Observable<GetFreelancerNotificationResponse> {
    return this.http.get<GetFreelancerNotificationResponse>(environment.routes.freelancer.notification.get);
  }
  
  public getLatestJobs(): Observable<GetFreelancerJobsResponse> {
    return this.http.get<GetFreelancerJobsResponse>(environment.routes.freelancer.job.latest);
  }
}
