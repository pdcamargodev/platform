import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetFreelancerProfile } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  public getProfile(scopes: string = "profile bank_account time_available financial"): Observable<GetFreelancerProfile> {
    return this.http.get<GetFreelancerProfile>(environment.routes.freelancer.profile.get + scopes)
  }
}
