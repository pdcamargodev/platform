// Angular
import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
// Lodash
import * as _ from 'lodash';
// RXJS
import { Subscription } from 'rxjs';
// Core
import { Freelancer } from '../../../../../core/_base/crud/models/freelancer';
import { PersonalInformationService } from './personal-information.service';
import { BaseResponse } from '../../../../../core/_base/crud/models/responses';

@Component({
  selector: 'kt-profile-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
  @Input('freelancer') freelancer: Freelancer;

  // Public params
  personalInformationForm: FormGroup;
  loading: boolean = false;

  // Private params
  private subscriptions: Subscription[] = []

  constructor(
    private titleService: Title,
    private cdr: ChangeDetectorRef,
    private personalInformationService: PersonalInformationService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    titleService.setTitle('Informações pessoais | _mybrief');
  }

  ngOnInit() {
    this.initLoginForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  /**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {
		this.personalInformationForm = this.fb.group({
			full_name: [this.freelancer.profile.name + ' ' + this.freelancer.profile.last_name, Validators.compose([
        Validators.required
      ])],
			location: [this.freelancer.profile.location],
			job: [this.freelancer.profile.job],
      cellphone: [this.freelancer.profile.cellphone],
      birthday: [this.freelancer.profile.birthday],
      email: [this.freelancer.email, Validators.compose([
          Validators.email
        ])
      ],
		});
	}

  submit() {
    if(this.loading) {
      return
    }

    const controls = this.personalInformationForm.controls;
		/** check form */
		if (this.personalInformationForm.invalid) {
      console.log('form invalid')
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }
    this.loading = true;

    const payload = {
      full_name: controls['full_name'].value,
			cellphone: controls['cellphone'].value,
			birthday: controls['birthday'].value,
			location: controls['location'].value,
			job: controls['job'].value,
			email: controls['email'].value,
    };

    if(payload.full_name == this.freelancer.profile.name + ' ' + this.freelancer.profile.last_name) {
      delete payload['full_name'];
    }

    if(payload.cellphone == this.freelancer.profile.cellphone) {
      delete payload['cellphone'];
    }

    if(payload.birthday == this.freelancer.profile.birthday) {
      delete payload['birthday'];
    }

    if(payload.location == this.freelancer.profile.location) {
      delete payload['location'];
    }

    if(payload.job == this.freelancer.profile.job) {
      delete payload['job'];
    }

    if(payload.email == this.freelancer.email) {
      delete payload['email'];
    }

    if(_.isEmpty(payload)) {
      this.snackBar.open('Perfil atualizado com sucesso', 'Entendi', {
        duration: 3000
      });
      this.loading = false;
      return
    }

    let subs = this.personalInformationService.updateProfile(payload)
      .subscribe((obj: BaseResponse) => {
        this.loading = false;
        this.cdr.markForCheck();

        this.snackBar.open(obj.message, 'Entendi', {
          duration: 3000
        });
      }, (error: HttpErrorResponse) => {
        if(error.status == 401 && error.error != null && error.error.statusCode == 401) {
          this.snackBar.open('Usuário ou senha inválida', 'Entendi', {
            duration: 3000
          });
        }

        this.loading = false;
        this.cdr.markForCheck();
      });

      this.subscriptions.push(subs);
  }

  newAvatarFile: File = null;
  previewFileSrc: string = "";
  isDeletingAvatar: boolean = false;
  onAvatarSelect(event: any): void {
    this.newAvatarFile = event.target['files'][0];
    this.previewFileSrc = window.URL.createObjectURL(this.newAvatarFile);
    this.isDeletingAvatar = false;
  }

  onCancelClick(): void {
    if(this.isDeletingAvatar) {
      this.isDeletingAvatar = false;
    } else {
      this.newAvatarFile = null;
      this.previewFileSrc = null;

      if(this.freelancer.profile.avatar != null) {
        this.isDeletingAvatar = true;
      }
    }

    this.cdr.markForCheck();
  }
}
