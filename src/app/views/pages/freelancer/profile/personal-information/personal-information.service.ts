import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonalInformationService {

  constructor(private http: HttpClient) {}

  updateProfile(payload: object): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.freelancer.profile.update, payload);
  }
}
