import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { ProfileService } from './profile.service';
import { Subscription } from 'rxjs';
import { GetFreelancerProfile } from '../../../../core/_base/crud/models/responses';
import { Freelancer } from '../../../../core/_base/crud/models/freelancer';

@Component({
  selector: 'kt-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private unsubscribe: Subscription[] = [];
  currentPage: string = "geral";
  freelancer: Freelancer = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private profileService: ProfileService,
    private cdr: ChangeDetectorRef
  ) {
    this.currentPage = this.route.snapshot.paramMap.get('page');

    let routerSubs = router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });

    this.unsubscribe.push(routerSubs);

    let profileSubs = profileService.getProfile().subscribe((getFreelancerProfile: GetFreelancerProfile) => {
      if(!getFreelancerProfile.error) {
        this.freelancer = getFreelancerProfile.data;
        this.cdr.markForCheck();
      }
    });

    this.unsubscribe.push(profileSubs);
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe.forEach(subs => subs.unsubscribe());
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
    }
    if (event instanceof NavigationEnd) {
      this.currentPage = this.route.snapshot.paramMap.get('page');
    }
  }

}
