import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild, ViewChildren, QueryList, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { JobService } from './jobs.service';
import { GetFreelancerJobsResponse, BaseResponse, JobAttachmentResponse, CreateCommentaryResponse } from '../../../../core/_base/crud/models/responses';
import { Job, JobCommentary } from '../../../../core/_base/crud/models/job';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { KanbanComponent } from '../../../../views/partials/content/widgets/kanban/kanban.component';
import { FreelancerRealtimeService } from '../../../../core/realtime';

@Component({
  selector: 'kt-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(KanbanComponent, {static: false}) kanban: KanbanComponent;

  private subscriptions: Subscription[] = [];

  console: Console = console;

  job: Job[] = [];
  isLoadingJob: boolean = false;
  selectedCompanies: number[] = [];
  orderBy: 'most_recent'|'less_recent'|'company'|'more_finish_date'|'less_finish_date' = 'most_recent';

  constructor(
    private titleService: Title,
    private jobService: JobService,
    private cdr: ChangeDetectorRef,
    private realTime: FreelancerRealtimeService
  ) {
    titleService.setTitle('Minhas tarefas | _mybrief');

    let subs = this.realTime.onAdminCommentInJob()
        .subscribe(data => {
          console.log(data.commentary);
        });

    this.subscriptions.push(subs);
  }

  ngOnInit(): void {
    this.getJobs();
  }

  ngAfterViewInit(): void {
    this.kanban.changeJobStatus.subscribe(async ({jobId, status, oldStatus, previousIndex, currentIndex}) => {
      await this.jobService.changeJobStatus(jobId, status).toPromise();
    });

    this.kanban.attachmentOutput.subscribe(async ({jobId, formData, skeletonIndex}) => {
      try {
        let obj: JobAttachmentResponse = await this.jobService.attachToJob(jobId, formData).toPromise();
        this.kanban.replaceSkeletonWithAttachment(obj.data, skeletonIndex);
      } catch (err) {
        // TODO: remover skeleton e voltar o card que estava escondido
      }
    });

    this.kanban.deleteAttachmentOutput.subscribe(async (attachmentId: number) => {
      try {
        await this.jobService.deleteAttachment(attachmentId).toPromise();
        this.kanban.removeAttachmentFromArrayOutput.emit(attachmentId);
      } catch (err) {
        // TODO: voltar o item que estava escondido / mensagem de erro
      }
    });

    this.kanban.newCommentOutput.subscribe(async ({jobId, commentary}) => {
      try {
        let obj: CreateCommentaryResponse = await this.jobService.commentInJob(jobId, commentary).toPromise();
        this.kanban.appendCommentaryOutput.emit(obj.data);
      } catch (err) {
        // TODO: voltar o item que estava escondido / mensagem de erro
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  async getJobs(hideOnLoading: boolean = true): Promise<void> {
    this.isLoadingJob = hideOnLoading;
    let freelancerJob: GetFreelancerJobsResponse = await this.jobService.getJobs({
      order_by: this.orderBy,
      companies: this.selectedCompanies
    }).toPromise();

    this.job = freelancerJob.data;
    this.isLoadingJob = false;

    this.cdr.markForCheck();
  }

  changeJobStatus(jobId: number, status: 'to_do'|'doing'|'to_approval'): void {
    let subs: Subscription = this.jobService.changeJobStatus(jobId, status)
      .subscribe((obj: BaseResponse) => {

      }, (error: HttpErrorResponse) => {

      });
  }
}
