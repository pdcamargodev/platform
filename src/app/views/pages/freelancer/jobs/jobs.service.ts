import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetFreelancerJobsResponse, BaseResponse, JobAttachmentResponse, CreateCommentaryResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';
import { FilterFreelancerJobsDto } from '../../../../core/_base/crud/models/requests';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  public getJobs(payload: FilterFreelancerJobsDto): Observable<GetFreelancerJobsResponse> {
    return this.http.post<GetFreelancerJobsResponse>(environment.routes.freelancer.job.get, payload);
  }

  public changeJobStatus(jobId: number, status: 'to_do'|'doing'|'to_approval'): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.freelancer.job.status.change.replace(':jobId', jobId.toString()), {
      status
    });
  }

  public attachToJob(jobId: number, formData: FormData): Observable<JobAttachmentResponse> {
    return this.http.post<JobAttachmentResponse>(environment.routes.common.upload.attachment.replace(':jobId', jobId.toString()), formData);
  }

  public deleteAttachment(attachmentId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.freelancer.job.attachment.delete.replace(':attachmentId', attachmentId.toString()));
  }

  public commentInJob(jobId: number, commentary: string): Observable<CreateCommentaryResponse> {
    return this.http.post<CreateCommentaryResponse>(environment.routes.freelancer.job.commentary.create.replace(':jobId', jobId.toString()), {
      commentary: commentary
    });
  }
}
