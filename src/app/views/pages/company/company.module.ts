import { NgModule } from '@angular/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Core Module
import { CompanyComponent } from './company.component';
import { RequestsComponent } from './requests/requests.component';
import { ProfileComponent } from './profile/profile.component';
import { FinancialComponent } from './financial/financial.component';
import { UsersComponent } from './users/users.component';
import { PartialsModule } from '../../partials/partials.module';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { DialogRateRequestComponent } from './requests/dialog-rate-request/dialog-rate-request.component';
import { CheckoutComponent } from './requests/checkout/checkout.component';
import { DialogNewRequestCategorySelectionComponent } from './requests/dialog-new-request-category-selection/dialog-new-request-category-selection.component';
import { ProductSelectionComponent } from './requests/product-selection/product-selection.component';
import { DialogSelectProductComponent } from './requests/product-selection/dialog-select-product/dialog-select-product.component';

@NgModule({
	declarations: [
		CompanyComponent,
		RequestsComponent,
		ProfileComponent,
		FinancialComponent,
		UsersComponent,
		DialogRateRequestComponent,
		CheckoutComponent,
		DialogNewRequestCategorySelectionComponent,
		ProductSelectionComponent,
		DialogSelectProductComponent
	],
	imports: [
		CommonModule,
		NgbTabsetModule,
		RouterModule.forChild([
			{
				path: ':page',
				component: CompanyComponent
			},
			{
				path: 'loja/:type/:category',
				component: ProductSelectionComponent
			},
			{
				path: '',
				redirectTo: '/empresa/geral',
				pathMatch: 'full'
			},
			{
				path: 'loja/checkout',
				component: CheckoutComponent,
				pathMatch: 'full'
			}
		]),
		PartialsModule,
		PerfectScrollbarModule,
		ReactiveFormsModule
	],
	entryComponents: [
		DialogRateRequestComponent,
		DialogNewRequestCategorySelectionComponent,
		DialogSelectProductComponent
	]
})
export class CompanyModule { }
