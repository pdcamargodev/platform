import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { GetCompanyRequestsPercentageResponse, GetCompanyAvailableCoinsAndConsumptionDataResponse } from '../../../../core/_base/crud/models/responses';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {

  constructor(private http: HttpClient) { }

  public getRequestsPercentage(): Observable<GetCompanyRequestsPercentageResponse> {
    return this.http.get<GetCompanyRequestsPercentageResponse>(environment.routes.company.job.request.percentage)
  }

  public getRequestsConsumption(): Observable<GetCompanyAvailableCoinsAndConsumptionDataResponse> {
    return this.http.get<GetCompanyAvailableCoinsAndConsumptionDataResponse>(environment.routes.company.job.request.consumption)
  }
}
