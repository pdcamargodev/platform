import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FinancialService } from './financial.service';
import { CompanyRequestsPercentage, CompanyAvailableCoinsAndConsumptionData } from '../../../../core/_base/crud/models/responses';

@Component({
  selector: 'kt-financial',
  templateUrl: './financial.component.html',
  styleUrls: ['./financial.component.scss']
})
export class FinancialComponent implements OnInit {

  public percentages: CompanyRequestsPercentage;
  public consumption: CompanyAvailableCoinsAndConsumptionData;
  public isLoading: boolean = false;
  public isLoadingConsumption: boolean = false;

  constructor(
    private readonly financialService: FinancialService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.getPercentages();
    this.getConsumptions();
  }

  private async getPercentages() {
    this.isLoading = true;
    this.percentages = (await this.financialService.getRequestsPercentage().toPromise()).data;

    this.isLoading = false;
    this.cdr.markForCheck();
  }

  private async getConsumptions() {
    this.isLoadingConsumption = true;
    this.consumption = (await this.financialService.getRequestsConsumption().toPromise()).data;

    this.isLoadingConsumption = false;
    this.cdr.markForCheck();
  }
}
