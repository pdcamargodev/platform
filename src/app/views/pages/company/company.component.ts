import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavigationEnd, RouterEvent, Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../core/_services';
import { CompanyUser } from '../../../core/_base/crud/models/company';
import { Subscription } from 'rxjs';

@Component({
  selector: 'kt-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  private unsubscribe: Subscription[] = [];
  currentPage: string = "geral";
  user: CompanyUser = null;

  validUrl: string[] = ['geral', 'pedidos', 'empresas', 'produtos', 'planos', 'pacotes', 'relatorios'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private authService: AuthService
  ) {
    this.user = authService.getCurrentUser().data;
    this.currentPage = this.route.snapshot.paramMap.get('page');

    if(!this.validUrl.includes(this.currentPage)) {
      this.router.navigateByUrl('/empresa');
    }

    let routerSubs = router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });

    this.unsubscribe.push(routerSubs);
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe.forEach(subs => subs.unsubscribe());
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationEnd) {
      this.currentPage = this.route.snapshot.paramMap.get('page');
    }
  }

}
