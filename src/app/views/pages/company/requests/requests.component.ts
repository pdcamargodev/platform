import { DialogNewRequestCategorySelectionComponent } from './dialog-new-request-category-selection/dialog-new-request-category-selection.component';
import { NgbActiveModal, NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { RequestService } from './request.service';

import { Job } from './../../../../core/_base/crud/models/job/job.model';

enum Tabs {
  DEVELOPMENT = 'development',
  SOCIAL_MEDIA = 'social_media',
  STRATEGY = 'strategy',
  CUSTOM = 'custom'
}

interface Jobs {
  social_media: JobsByCategory
  development: JobsByCategory
  strategy: JobsByCategory
  custom: JobsByCategory
}

interface JobsByCategory {
	to_do: Job[]
	doing: Job[]
	to_approval: Job[]
	to_change: Job[]
	done: Job[]
}

interface RateJobPayload {
	jobId: number
	requestId: number
	status: 'approval'|'disapproval'
	feedback: string
}

interface RateJobEvent {
	payload: RateJobPayload,
	activeModal: NgbActiveModal
}

import Swal from 'sweetalert2';

@Component({
  selector: 'kt-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
	console = console;
	Tabs = Tabs;
	currentTab: Tabs = Tabs.SOCIAL_MEDIA;

	modalRef: NgbModalRef;

	loadedCategories: Tabs[] = []

	jobs: Jobs

	constructor(
		private readonly requestService: RequestService,
		private cdr: ChangeDetectorRef,
		private snackBar: MatSnackBar,
		private modalService: NgbModal
	) { }

	ngOnInit() {
		this.initEmptyJobs();
		this.setCurrentTab(Tabs.SOCIAL_MEDIA);
	}

	initEmptyJobs(): void {
		const emptyJobs: JobsByCategory = {
		to_do: [],
		doing: [],
		to_approval: [],
		to_change: [],
		done: [],
		}

		this.jobs = {
		social_media: emptyJobs,
		development: emptyJobs,
		strategy: emptyJobs,
		custom: emptyJobs,
		}
	}

	async rateJob(data: RateJobEvent): Promise<void> {
		Swal.fire({
			icon: 'info',
			title: 'Enviando sua avaliação',
			text: 'Estamos registrando o seu feedback para melhorias futuras de nossas entregas.',
			allowOutsideClick: false,
			allowEscapeKey: false,
			showCloseButton: false
		})

		const result = await this.requestService.rateRequest(data.payload).toPromise();

		if(result.error) {
			Swal.fire({
				title: 'Erro',
				icon: 'question',
				text: 'Houve um erro ao enviar a sua avaliação. Por favor, tente novamente mais tarde.',
				buttonsStyling: false,
				customClass: {
					confirmButton: 'btn btn-primary'
				},
				confirmButtonText: 'Entendi',
			})
			return;
		}

		Swal.fire({
			title: 'Finalizado',
			icon: 'success',
			text: `Sua avaliação foi registrada e a entrega foi ${status === 'approval' ? 'aprovada' : 'desaprovada'} com sucesso.`,
			buttonsStyling: false,
			customClass: {
				confirmButton: 'btn btn-primary'
			},
			confirmButtonText: 'Entendi',
		});

		data.activeModal.dismiss('Request Component');

		this.loadJobByCategory(this.currentTab, true);
	}

	setCurrentTab(tab: Tabs): void {
		this.currentTab = tab;

		this.loadJobByCategory(tab)
	}

	async loadJobByCategory(tab: Tabs, force = false): Promise<void> {
		if(this.loadedCategories.includes(tab) && force === false) { return }

		const { data, error, message } = await this.requestService.getRequests(tab).toPromise();

		if(!error) {
			if(!this.loadedCategories.includes(tab)) {
				this.loadedCategories.push(tab)
			}

			const jobByCategory = this.splitJobsByStatus(data);
			this.jobs[tab] = jobByCategory;
			this.cdr.markForCheck();
		} else {
			this.snackBar.open(message, 'Entendi', {
				duration: 3000
			})
		}
	}

	splitJobsByStatus(jobs: Job[]): JobsByCategory {
		const jobByCategory = {
			doing: jobs.filter(job => job.status === 'doing'),
			to_do: jobs.filter(job => job.status === 'to_do'),
			done: jobs.filter(job => job.status === 'done'),
			to_approval: jobs.filter(job => job.status === 'to_approval'),
			to_change: jobs.filter(job => job.status === 'to_change')
		}

		return jobByCategory;
	}

	openCategorySelectionDialog(): void {
		this.modalRef = this.modalService.open(DialogNewRequestCategorySelectionComponent, {
			centered: true,
			size: 'lg'
		});
	}
}
