import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-dialog-new-request-category-selection',
  templateUrl: './dialog-new-request-category-selection.component.html',
  styleUrls: ['./dialog-new-request-category-selection.component.scss']
})
export class DialogNewRequestCategorySelectionComponent implements OnInit {

	constructor(
		private router: Router,
		public activeModal: NgbActiveModal
	) { }

	ngOnInit() {
	}

	onCategoryClick(url: string): void {
		this.activeModal.dismiss('Category selected');
		this.router.navigateByUrl('/empresa/loja/' + url);
	}
}
