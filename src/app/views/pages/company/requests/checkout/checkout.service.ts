import { environment } from './../../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GetCompanyBillingAddress, BaseResponse, CreateCompanyInvoice } from './../../../../../core/_base/crud/models';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(
	  private http: HttpClient
  ) { }

  async getCompanyAddress() {
	this.http.get<GetCompanyBillingAddress>(environment.routes.company.address);
  }

  async createCompanyInvoice(param: CreateCompanyInvoice) {
	return await this.http.post<BaseResponse>(environment.routes.company.invoice.create, param).toPromise<BaseResponse>();
  }
}
