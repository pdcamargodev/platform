import { Component, ChangeDetectorRef, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogNewRequestCategorySelectionComponent } from './../dialog-new-request-category-selection/dialog-new-request-category-selection.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CartService, CartItem } from './../../../../../core/_services/cart.service';
import { Location } from '@angular/common';
import { CreateCompanyInvoice } from './../../../../../core/_base/crud/models';
import { CheckoutService } from './checkout.service';

import { removeAllWhiteSpace } from '../../../../../core';

declare let Card: any;

@Component({
  selector: 'kt-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements AfterViewInit, OnInit {
	@ViewChild('formWrapper', { static: false }) formWrapper: ElementRef;

	cardForm: FormGroup;
	billingForm: FormGroup;
  	loading: boolean = false;

	items: CartItem[] = [];
	cashPrice: number = 0;
	coinPrice: number = 0;

	modalRef: NgbModalRef;

	paymentType: "credit_card"|"bank_slip" = null;

	constructor(
		private cartService: CartService,
		private cdr: ChangeDetectorRef,
		private modalService: NgbModal,
		private router: Router,
		private location: Location,
		private checkoutService: CheckoutService,
		private fb: FormBuilder
	) {
		this.items = cartService.getCartItems();

		this.setPricesValues();

		this.cartService.items.subscribe(items => {
			this.items = items;

			this.setPricesValues();

			this.cdr.markForCheck();
		});
	}

	ngAfterViewInit() {
		const form = this.formWrapper.nativeElement;
		new Card({
            form: form,
			container: '.card-wrapper',
			placeholders: {
				number: '•••• •••• •••• ••••',
				name: 'Nome Completo',
				expiry: '••/••',
				cvc: '•••'
			}
        });
	}

	ngOnInit() {
		this.initCardForm();
		this.initBillingForm();
	}

	private initCardForm(): void {
		this.cardForm = this.fb.group({
		  cardNumber: ['', Validators.compose([
			Validators.required,
		  ])],
		  cardName: ['', Validators.compose([
			Validators.required,
		  ])],
		  cardCvv: ['', Validators.compose([
			Validators.required
		  ])],
		  cardDate: ['', Validators.compose([
			Validators.required,
		  ])]
		});
	}

	private initBillingForm(): void {
		this.billingForm = this.fb.group({
		  installments: ['1'],
		  fullName: ['', Validators.compose([
			Validators.required,
		  ])],
		  cellphone: ['', Validators.compose([
			Validators.required,
		  ])],
		  document: ['', Validators.compose([
			Validators.required,
		  ])],
		  birthday: ['', Validators.compose([
			Validators.required,
		  ])],
		  zipcode: ['', Validators.compose([
			Validators.required
		  ])],
		  street: ['', Validators.compose([
			Validators.required,
		  ])],
		  number: ['', Validators.compose([
			Validators.required,
		  ])],
		  complementary: ['', Validators.compose([
			Validators.required,
		  ])],
		  neighborhood: ['', Validators.compose([
			Validators.required,
		  ])],
		  city: ['', Validators.compose([
			Validators.required,
		  ])],
		  state: ['', Validators.compose([
			Validators.required,
		  ])]
		});
	}

	hasCardError(controlName: string, validationType: string): boolean {
		const control = this.cardForm.controls[controlName];
		if (!control) {
			return false;
    	}
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	hasBillingError(controlName: string, validationType: string): boolean {
		const control = this.billingForm.controls[controlName];
		if (!control) {
			return false;
    	}
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	private setPricesValues() {
		const { cash, coin } = this.cartService.getCartTotalPrice();

		this.cashPrice = cash;
		this.coinPrice = coin;
	}

	setItemQuantity(index: number, quantity: number): void {
		if(quantity <= 0) {
			this.cartService.removeItem(index);
		} else {
			this.cartService.setItemQuantity(index, quantity);
		}
	}

	removeItem(index: number): void {
		this.cartService.removeItem(index);
	}

	toInt(value: string) {
		return Number(value);
	}

	openCategorySelectionDialog(): void {
		this.modalRef = this.modalService.open(DialogNewRequestCategorySelectionComponent, {
			centered: true,
			size: 'lg'
		});
	}

	redirectToOrderList() {
		this.router.navigateByUrl('/empresa/pedidos');
	}

	goBack() {
		this.location.back()
	}

	getCard() {
		if(this.loading) {
			return null
		}

		const cardControl = this.cardForm.controls;

		/** check form */
		if (this.cardForm.invalid) {
			Object.keys(cardControl).forEach(controlName =>
				cardControl[controlName].markAsTouched()
			);
			return null;
		}

		const cardPayload = {
			card_number: removeAllWhiteSpace(cardControl['cardNumber'].value),
			card_holder_name: cardControl['cardName'].value,
			card_cvv: cardControl['cardCvv'].value,
			card_expiration_date: removeAllWhiteSpace(cardControl['cardDate'].value),
		}

		return cardPayload;
	}

	getBilling(cardName: string) {
		if(this.loading) {
			return null
		}

		const billingControl = this.billingForm.controls;

		/** check form */
		if (this.billingForm.invalid) {
			Object.keys(billingControl).forEach(controlName => {
				console.log(billingControl[controlName]);
				billingControl[controlName].markAsTouched()
			}
			);
			return null;
		}

		const billingPayload = {
			billing: {
				name: cardName,
				address: {
					street: billingControl['street'].value,
					street_number: billingControl['number'].value,
					zipcode: billingControl['zipcode'].value,
					country: 'br',
					state: billingControl['state'].value,
					city: billingControl['city'].value,
					neighborhood: billingControl['neighborhood'].value,
					complementary: billingControl['complementary'].value,
				}
			},
			customer: {
				name: billingControl['fullName'].value,
				document: [
					{
						type: 'cpf',
						number: billingControl['document'].value
					}
				],
				phone_numbers: [billingControl['cellphone'].value],
				birthday: billingControl['birthday'].value,
				country: 'br',
				type: 'individual'
			},
			installments: billingControl['installments'].value,
			payment_method: this.paymentType === 'credit_card' ? 'credit_card' : 'boleto'
		}

		return billingPayload;
	}


	getItems() {
		const items: CreateCompanyInvoice['invoice_item'] = [];

		this.items.map(item => {
			items.push({
				item_type: item.type,
				item_id: item.item.id,
				quantity: item.quantity,
				payment_type: this.paymentType
			})
		});

		return items;
	}

	async confirmOrder() {
		const card = this.getCard();

		if(!card) {
			return;
		}

		const billing = this.getBilling(card.card_holder_name);

		if(!billing) {
			return;
		}

		const items = this.getItems();

		if(items.length === 0) {
			return;
		}

		const params = {
			invoice_item: items,
			pagarme_info: {
				...(this.paymentType === 'credit_card' && {card}),
				...billing,
				postback_url: ''
			},
			price: this.cashPrice,
		}

		const { error } = await this.checkoutService.createCompanyInvoice(params as any);

		console.log(error);
	}
}
