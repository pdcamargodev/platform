import { BaseResponse } from './../../../../core/_base/crud/models/responses/base-response';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GetCompanyJobs } from './../../../../core/_base/crud/models/responses';
import { environment } from './../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http: HttpClient) { }

  public getRequests(category: 'social_media'|'development'|'strategy'|'custom' = 'social_media'): Observable<GetCompanyJobs> {
	return this.http.get<GetCompanyJobs>(environment.routes.company.job.list.by_category.replace(':category', category))
  }

  public rateRequest({ jobId, requestId, status, feedback }): Observable<BaseResponse> {
	  return this.http.post<BaseResponse>(environment.routes.company.job.feedback, {
		  job_id: jobId,
		  company_job_feedback_request_id: requestId,
		  status,
		  feedback
	  });
  }
}
