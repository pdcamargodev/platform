import { DialogSelectProductComponent } from './dialog-select-product/dialog-select-product.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductSelectionService } from './product-selection.service';
import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { FilterCompanyProductDto, FilterCompanyPackageDto, FilterCompanyPlanDto } from './../../../../../core/_base/crud/models/requests';
import { Product, Package, Plan } from './../../../../../core/_base/crud/models';

type FilterType = FilterCompanyProductDto | FilterCompanyPackageDto | FilterCompanyPlanDto;
type CategoryType = FilterCompanyProductDto['category'];
type DataType = Product[] | Package[] | Plan[];
type Types = 'produtos'|'pacotes'|'planos';
type Categories = 'desenvolvimento'|'social-media'|'estrategia';

@Component({
  selector: 'kt-product-selection',
  templateUrl: './product-selection.component.html',
  styleUrls: ['./product-selection.component.scss']
})
export class ProductSelectionComponent implements OnInit {
	validCategories = {
		'social-media': 'social_media',
		desenvolvimento: 'development',
		estrategia: 'strategy',
		'': ''
	};
	validTypes = {
		produtos: 'getProducts',
		planos: 'getPlans',
		pacotes: 'getPackages'
	};
	selectedCategory: CategoryType;
	selectedItemType: string;

	typeName: Types;
	categoryName: Categories;

	searchQuery: string = "";

	data: DataType = [];

	modalRef: NgbModalRef;

    constructor(
		private productSelectionService: ProductSelectionService,
		private route: ActivatedRoute,
		private router: Router,
		private cdr: ChangeDetectorRef,
		private modalService: NgbModal
	) {
		this.typeName = route.snapshot.paramMap.get('type') as Types;
		this.categoryName = route.snapshot.paramMap.get('category') as Categories;

		this.selectedCategory = this.validCategories[this.categoryName as string];
		this.selectedItemType = this.validTypes[this.typeName];

		if(!this.selectedCategory || !this.selectedItemType) {
			router.navigateByUrl('/empresa/pedidos');
			return;
		}

		route.queryParams.subscribe(params => {
			const { q } = params;

			if(q) {
				this.searchQuery = q;
			} else {
				this.searchQuery = "";
				this.router.navigate([]);
			}
		});
	}

	ngOnInit() {
		this.getItems();
	}

	onFilter({ category, type, query }: {category: Categories, type: Types, query: string}) {
		this.router.navigate([`/empresa/loja/${type}/${category}`], {
			queryParams: {
				q: query
			},
			queryParamsHandling: 'merge'
		});

		this.selectedCategory = this.validCategories[category as string];
		this.selectedItemType = this.validTypes[type];

		if(!this.selectedCategory || !this.selectedItemType) {
			this.router.navigateByUrl('/empresa/pedidos');
			return;
		}

		if(query) {
			this.searchQuery = query;
		} else {
			this.searchQuery = "";
		}

		this.cdr.markForCheck();

		this.getItems();
	}

    async getItems(): Promise<void> {
		const { error, message, data } = await this.productSelectionService[this.selectedItemType](this.getFilters()).toPromise();

		this.data = data;
		this.cdr.markForCheck();
	}

	getFilters(): FilterType {
		return {
			...(this.selectedItemType === 'getProducts' && {
				category: this.selectedCategory
			}),
			...(this.searchQuery.trim() !== '' && {
				name: this.searchQuery.trim()
			})
		}
	}

	openSelectItemDialog(item: DataType[number], type: 'product'|'plan'|'package'): void {
		this.modalRef = this.modalService.open(DialogSelectProductComponent, {
			centered: true,
			size: 'lg'
		});
		this.modalRef.componentInstance.item = item;
		this.modalRef.componentInstance.type = type;

		this.modalRef.result.then((a) => {
			console.log('When user closes', a);
		}, (a) => {
			console.log('Backdrop click', a)
		}
		)
	}
}
