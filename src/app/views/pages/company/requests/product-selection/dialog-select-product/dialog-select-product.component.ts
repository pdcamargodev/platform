import { Component, OnInit } from '@angular/core';
import { Product, Package, Plan } from './../../../../../../core/_base/crud/models';
import { CartService } from './../../../../../../core/_services/cart.service';

@Component({
  selector: 'kt-dialog-select-product',
  templateUrl: './dialog-select-product.component.html',
  styleUrls: ['./dialog-select-product.component.scss']
})
export class DialogSelectProductComponent implements OnInit {
	type: 'product'|'plan'|'package';
	item: Product|Package|Plan;

	constructor(private cartService: CartService) { }

	ngOnInit() {
	}

	addToCart(quantity: number) {
		this.cartService.addCartItems([{
			quantity,
			type: this.type,
			item: this.item
		}])
	}
}
