import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetProductsResponse, GetPackagesResponse, GetPlansResponse } from './../../../../../core/_base/crud/models/responses';
import { FilterCompanyProductDto, FilterCompanyPackageDto, FilterCompanyPlanDto } from './../../../../../core/_base/crud/models/requests';
import { environment } from './../../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductSelectionService {

	constructor(private http: HttpClient) { }

	getProducts(filters: FilterCompanyProductDto): Observable<GetProductsResponse> {
		return this.http.post<GetProductsResponse>(environment.routes.company.store.product, filters);
	}

	getPackages(filters: FilterCompanyPackageDto): Observable<GetPackagesResponse> {
		return this.http.post<GetPackagesResponse>(environment.routes.company.store.package, filters);
	}

	getPlans(filters: FilterCompanyPlanDto): Observable<GetPlansResponse> {
		return this.http.post<GetPlansResponse>(environment.routes.company.store.plan, filters);
	}
}
