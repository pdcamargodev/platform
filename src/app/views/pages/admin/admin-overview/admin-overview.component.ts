import { Component, OnInit } from '@angular/core';
import { Widget1Data } from '../../../../views/partials/content/widgets/widget1/widget1.component';

@Component({
  selector: 'kt-admin-overview',
  templateUrl: './admin-overview.component.html',
  styleUrls: ['./admin-overview.component.scss']
})
export class AdminOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  widget1Data: Widget1Data[] = [
    {
      title: 'Desenvolvimento',
      desc: 'Vendas no mês',
      value: 'R$ 7.500',
      valueClass: 'kt-font-brand'
    },
    {
      title: 'Social media',
      desc: 'Vendas no mês',
      value: 'R$ 2.500',
      valueClass: 'kt-font-success'
    },
    {
      title: 'Estratégia',
      desc: 'Vendas no mês',
      value: 'R$ 4.500',
      valueClass: 'kt-font-danger'
    },
  ]
}
