import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { JobsComponent as AdminJobsComponent } from './jobs/jobs.component';
import { RouterModule } from '@angular/router';
import { AdminJobsModule } from './jobs/jobs.module';
import { PartialsModule } from '../../partials/partials.module';
import { AdminOverviewComponent } from './admin-overview/admin-overview.component';
import { AdminProductComponent } from './admin-product/admin-product.component';
import { AdminFreelancerComponent } from './admin-freelancer/admin-freelancer.component';
import { AdminCompanyComponent } from './admin-company/admin-company.component';
import { AdminReportComponent } from './admin-report/admin-report.component';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { DialogEditCompanyComponent } from './admin-company/dialog-edit-company/dialog-edit-company.component';
import { DialogCompanyRequestComponent } from './admin-company/dialog-company-request/dialog-company-request.component';
import { DialogEditProductComponent } from './admin-product/dialog-edit-product/dialog-edit-product.component';
import { DialogEditFreelancerComponent } from './admin-freelancer/dialog-edit-freelancer/dialog-edit-freelancer.component';
import { DialogFreelancerNewTaskComponent } from './admin-freelancer/dialog-freelancer-new-task/dialog-freelancer-new-task.component';
import { AdminPackageComponent } from './admin-package/admin-package.component';
import { AdminPlanComponent } from './admin-plan/admin-plan.component';
import { DialogEditPackageComponent } from './admin-package/dialog-edit-package/dialog-edit-package.component';
import { DialogEditPlanComponent } from './admin-plan/dialog-edit-plan/dialog-edit-plan.component';
import { CoreModule } from '../../../core/core.module';
import { MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdminComponent,
    AdminOverviewComponent,
    AdminProductComponent,
    AdminFreelancerComponent,
    AdminCompanyComponent,
    AdminReportComponent,
    DialogEditCompanyComponent,
    DialogCompanyRequestComponent,
    DialogEditProductComponent,
    DialogEditFreelancerComponent,
    DialogFreelancerNewTaskComponent,
    AdminPackageComponent,
    AdminPlanComponent,
    DialogEditPackageComponent,
    DialogEditPlanComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    PartialsModule,
    AdminJobsModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: ':page',
        component: AdminComponent
      },
      {
				path: '',
        redirectTo: '/administrador/geral',
        pathMatch: 'full'
      }
    ]),
    NgbDropdownModule,
    NgbModalModule
  ],
  entryComponents: [
    DialogEditCompanyComponent,
    DialogCompanyRequestComponent,
    DialogEditProductComponent,
    DialogEditFreelancerComponent,
    DialogFreelancerNewTaskComponent,
    DialogEditPackageComponent,
    DialogEditPlanComponent
  ]
})
export class AdminModule { }
