import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Package } from '../../../../core/_base/crud/models/package';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogEditPackageComponent } from './dialog-edit-package/dialog-edit-package.component';
import { AdminPackageService } from './admin-package.service';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'kt-admin-package',
  templateUrl: './admin-package.component.html',
  styleUrls: ['./admin-package.component.scss']
})
export class AdminPackageComponent implements OnInit {
  // Private params
  private modalRef: NgbModalRef;

  // Public params
  public isLoading: boolean = false;
  public packages: Package[] = [];

  public packageBeingDeleted: number[] = [];

  constructor(
    private modalService: NgbModal,
    private readonly adminPackageService: AdminPackageService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getPackages();
  }

  private async getPackages() {
    this.isLoading = true;

    this.packages = (await this.adminPackageService.getPackages(null).toPromise()).data;

    this.isLoading = false;

    this.cdr.markForCheck();
  }

  openEditPackageDialog(item: Package): void {
    this.modalRef = this.modalService.open(DialogEditPackageComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.package = item;
  }

  async deletePackage(packageId: number) {
    Swal.fire({
      icon: 'warning',
      title: 'Atenção',
      html: 'Você tem certeza que deseja excluir o pacote <b>'+this.packages[this.packages.findIndex(f => f.id == packageId)].name+'</b>? Esta ação é irreversível e nenhuma empresa poderá mais comprar este pacote.',
      reverseButtons: true,
      showCancelButton: true,
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.callDeleteFunction(packageId);
      }
    });
  }

  async callDeleteFunction(packageId: number) {
    this.packageBeingDeleted.push(packageId);
    let {error} = await this.adminPackageService.softDeletePackage(packageId).toPromise();

    this.packageBeingDeleted.splice(this.packageBeingDeleted.findIndex(id => id == packageId), 1);

    this.snackBar.open(error ? 'Erro ao excluir o pacote' : 'Pacote excluído com sucesso', 'Entendi', {
      duration: 3000
    });

    if(!error) {
      this.packages.splice(this.packages.findIndex(f => f.id == packageId), 1);
    }


    this.cdr.markForCheck();
  }
}
