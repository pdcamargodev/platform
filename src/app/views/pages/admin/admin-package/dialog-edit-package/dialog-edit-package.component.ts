import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Package } from '../../../../../core/_base/crud/models/package';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { AdminPackageService } from '../admin-package.service';

@Component({
  selector: 'kt-dialog-edit-package',
  templateUrl: './dialog-edit-package.component.html',
  styleUrls: ['./dialog-edit-package.component.scss']
})
export class DialogEditPackageComponent implements OnInit {

  // Public Params
  editPackageForm: FormGroup;
  loading: boolean = false;

  @Input() package: Package;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private adminPackageService: AdminPackageService
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  private initForm(): void {
    this.editPackageForm = this.fb.group({
			name: [this.package.name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      description: [this.package.description],
      priceCoin: [this.package.price_coin],
      price: [this.package.price]
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.editPackageForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
  }

  async submit() {
    if(this.loading) {
      return
    }

    const controls = this.editPackageForm.controls;
		/** check form */
		if (this.editPackageForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    const payload = {
      name: controls['name'].value,
      description: controls['description'].value,
      price: controls['price'].value,
      price_coin: controls['priceCoin'].value
    }

    if((payload.price == null || payload.price.toString().trim() == '') && payload.price_coin == null) {
      return this.snackBar.open('Pelo menos um tipo de preço é necessário', 'Entendi', {
        duration: 3000
      })
    }

    if(payload.name == this.package.name) {
      delete payload['name'];
    }

    if(payload.description == this.package.description) {
      delete payload['description'];
    }

    if(payload.price == this.package.price) {
      delete payload['price'];
    }

    if(payload.price_coin == this.package.price_coin) {
      delete payload['price_coin'];
    }

    this.loading = true;

    if(Object.keys(payload).length == 0) {
      this.loading = false;
      this.snackBar.open('Pacote atualizado com sucesso', 'Entendi', {
        duration: 3000
      });

      return;
    }

    let {error} = await this.adminPackageService.updatePackage(this.package.id, payload).toPromise();

    this.snackBar.open(error? 'Erro ao atualizar as informações do pacote' : 'Pacote atualizado com sucesso', 'Entendi', {
      duration: 3000
    });

    this.loading = false;

    this.cdr.markForCheck();
  }

}
