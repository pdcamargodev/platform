import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GetPackagesResponse, BaseResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminPackageService {

  constructor(private http: HttpClient) { }

  getPackages(payload): Observable<GetPackagesResponse> {
    return this.http.post<GetPackagesResponse>(environment.routes.admin.package.get, payload);
  }

  softDeletePackage(packageId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.package.delete.replace(':packageId', packageId.toString()));
  }

  updatePackage(packageId: number, payload: any): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.package.update.replace(':packageId', packageId.toString()), payload)
  }
}
