import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Product } from '../../../../../core/_base/crud/models/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AdminProductService } from '../admin-product.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'kt-dialog-edit-product',
  templateUrl: './dialog-edit-product.component.html',
  styleUrls: ['./dialog-edit-product.component.scss']
})
export class DialogEditProductComponent implements OnInit {
  // Public Params
  editProductForm: FormGroup;
  loading: boolean = false;

  @Input() product: Product;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private adminProductService: AdminProductService
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  private initForm(): void {
    this.editProductForm = this.fb.group({
			name: [this.product.name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      description: [this.product.description],
      category: [this.product.category, Validators.compose([
        Validators.required
      ])],
      priceCoin: [this.product.price_coin],
      price: [this.product.price]
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.editProductForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
  }

  async submit() {
    if(this.loading) {
      return
    }

    const controls = this.editProductForm.controls;
		/** check form */
		if (this.editProductForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    const payload = {
      name: controls['name'].value,
      description: controls['description'].value,
      category: controls['category'].value,
      price: controls['price'].value,
      price_coin: controls['priceCoin'].value
    }

    if((payload.price == null || payload.price.trim() == '') && (payload.price_coin == null || payload.price_coin.trim() == '')) {
      return this.snackBar.open('Pelo menos um tipo de preço é necessário', 'Entendi', {
        duration: 3000
      })
    }

    if(payload.name == this.product.name) {
      delete payload['name'];
    }

    if(payload.description == this.product.description) {
      delete payload['description'];
    }

    if(payload.category == this.product.category) {
      delete payload['category'];
    }

    if(payload.price == this.product.price) {
      delete payload['price'];
    }

    if(payload.price_coin == this.product.price_coin) {
      delete payload['price_coin'];
    }

    this.loading = true;

    if(Object.keys(payload).length == 0) {
      this.loading = false;
      this.snackBar.open('Produto atualizado com sucesso', 'Entendi', {
        duration: 3000
      });

      return;
    }

    let {error} = await this.adminProductService.updateProduct(this.product.id, payload).toPromise();

    this.snackBar.open(error? 'Erro ao atualizar as informações do produto' : 'Produto atualizado com sucesso', 'Entendi', {
      duration: 3000
    });

    this.loading = false;

    this.cdr.markForCheck();
  }
}
