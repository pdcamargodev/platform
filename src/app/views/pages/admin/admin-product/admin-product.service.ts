import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetProductsResponse, BaseResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminProductService {

  constructor(private http: HttpClient) { }

  getProducts(payload): Observable<GetProductsResponse> {
    return this.http.post<GetProductsResponse>(environment.routes.admin.product.get, payload);
  }

  softDeleteProduct(productId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.product.delete.replace(':productId', productId.toString()));
  }

  updateProduct(productId: number, payload: any): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.product.update.replace(':productId', productId.toString()), payload)
  }
}
