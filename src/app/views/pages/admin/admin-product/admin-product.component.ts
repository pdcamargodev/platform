import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from '../../../../core/_base/crud/models/product';
import { DialogEditProductComponent } from './dialog-edit-product/dialog-edit-product.component';
import { AdminProductService } from './admin-product.service';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'kt-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.scss']
})
export class AdminProductComponent implements OnInit {
  // Privates params
  private modalRef: NgbModalRef;

  // Public params
  public isLoading: boolean = false;
  public products: Product[] = [];

  public productBeingDeleted: number[] = [];

  constructor(
    private modalService: NgbModal,
    private readonly adminProductService: AdminProductService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getProducts();
  }

  private async getProducts() {
    this.isLoading = true;

    this.products = (await this.adminProductService.getProducts(null).toPromise()).data;

    this.isLoading = false;

    this.cdr.markForCheck();
  }

  openEditProductDialog(product: Product): void {
    this.modalRef = this.modalService.open(DialogEditProductComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.product = product;
  }

  async deleteProduct(productId: number) {
    Swal.fire({
      icon: 'warning',
      title: 'Atenção',
      html: 'Você tem certeza que deseja excluir o produto <b>'+this.products[this.products.findIndex(f => f.id == productId)].name+'</b>? Esta ação é irreversível e nenhuma empresa poderá mais comprar este produto.',
      reverseButtons: true,
      showCancelButton: true,
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.callDeleteFunction(productId);
      }
    });
  }

  async callDeleteFunction(productId: number) {
    this.productBeingDeleted.push(productId);
    let {error} = await this.adminProductService.softDeleteProduct(productId).toPromise();

    this.productBeingDeleted.splice(this.productBeingDeleted.findIndex(id => id == productId), 1);

    this.snackBar.open(error ? 'Erro ao excluir o produto' : 'Pacote excluído com sucesso', 'Entendi', {
      duration: 3000
    });

    if(!error) {
      this.products.splice(this.products.findIndex(f => f.id == productId), 1);
    }


    this.cdr.markForCheck();
  }
}
