import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsComponent as AdminJobsComponent } from './jobs.component';
import { PartialsModule } from '../../../../views/partials/partials.module';
import { ThemeModule } from '../../../../views/theme/theme.module';
import { WidgetModule } from '../../../../views/partials/content/widgets/widget.module';

@NgModule({
  declarations: [AdminJobsComponent],
  imports: [
    CommonModule,
    PartialsModule,
    ThemeModule,
    WidgetModule
  ],
  exports: [AdminJobsComponent]
})
export class AdminJobsModule { }
