import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { GetAdminJobsResponse, BaseResponse, JobAttachmentResponse, CreateCommentaryResponse } from '../../../../core/_base/crud/models/responses';
import { FilterAdminJobsDto } from '../../../../core/_base/crud/models/requests';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  constructor(private http: HttpClient) { }

  public getJobs(payload: FilterAdminJobsDto): Observable<GetAdminJobsResponse> {
    return this.http.post<GetAdminJobsResponse>(environment.routes.admin.job.get, payload);
  }

  public changeJobStatus(jobId: number, status: 'to_do'|'doing'|'to_approval'|'done'|'to_change'): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.job.status.change.replace(':jobId', jobId.toString()), {
      status
    });
  }

  public changeJobDeadline(jobId: number, startDate: Date, finishDate: Date): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.job.deadline.change.replace(':jobId', jobId.toString()), {
      start_date: startDate,
      finish_date: finishDate
    });
  }

  public attachToJob(jobId: number, formData: FormData): Observable<JobAttachmentResponse> {
    return this.http.post<JobAttachmentResponse>(environment.routes.common.upload.admin.attachment.replace(':jobId', jobId.toString()), formData);
  }

  public deleteAttachment(attachmentId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.job.attachment.delete.replace(':attachmentId', attachmentId.toString()));
  }

  public commentInJob(jobId: number, commentary: string): Observable<CreateCommentaryResponse> {
    return this.http.post<CreateCommentaryResponse>(environment.routes.admin.job.commentary.create.replace(':jobId', jobId.toString()), {
      commentary: commentary
    });
  }
}
