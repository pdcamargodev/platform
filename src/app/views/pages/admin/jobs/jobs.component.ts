import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { AdminKanbanComponent } from '../../../../views/partials/content/widgets/admin-kanban/admin-kanban.component';
import { Subscription } from 'rxjs';
import { Job } from '../../../../core/_base/crud/models/job';
import { Title } from '@angular/platform-browser';
import { JobsService } from './jobs.service';
import { GetAdminJobsResponse, CreateCommentaryResponse, JobAttachmentResponse } from '../../../../core/_base/crud/models/responses';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'kt-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit, AfterViewInit {
  @ViewChild(AdminKanbanComponent, {static: false}) kanban: AdminKanbanComponent;

  private subscriptions: Subscription[] = [];

  console: Console = console;

  job: Job[] = [];
  isLoadingJob: boolean = false;
  selectedCompanies: number[] = [];
  selectedFreelancers: number[] = [];
  orderBy: 'most_recent'|'less_recent'|'company'|'more_finish_date'|'less_finish_date' = 'most_recent';

  constructor(
    private titleService: Title,
    private jobService: JobsService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute
  ) {
    titleService.setTitle('Tarefas | _mybrief');
    route.queryParams.subscribe(params => {
      if(params['freelancer'] != null) {
        this.selectedFreelancers.push(parseInt(params['freelancer']));
      }
    });
  }

  ngOnInit() {
    this.getJobs();
  }

  ngAfterViewInit(): void {
    this.kanban.changeDeadlineOutput.subscribe(async ({jobId, startDate, finishDate}) => {
      await this.jobService.changeJobDeadline(jobId, startDate, finishDate).toPromise();
      this.kanban.updateDeadlineFrontOutput.emit({jobId, startDate, finishDate});
    });

    this.kanban.changeJobStatus.subscribe(async ({jobId, status, oldStatus, previousIndex, currentIndex}) => {
      await this.jobService.changeJobStatus(jobId, status).toPromise();
    });

    this.kanban.attachmentOutput.subscribe(async ({jobId, formData, skeletonIndex}) => {
      try {
        let obj: JobAttachmentResponse = await this.jobService.attachToJob(jobId, formData).toPromise();
        // this.kanban.replaceSkeletonWithAttachment(obj.data, skeletonIndex);
      } catch (err) {
        // TODO: remover skeleton e voltar o card que estava escondido
      }
    });

    this.kanban.deleteAttachmentOutput.subscribe(async (attachmentId: number) => {
      try {
        await this.jobService.deleteAttachment(attachmentId).toPromise();
        this.kanban.removeAttachmentFromArrayOutput.emit(attachmentId);
      } catch (err) {
        // TODO: voltar o item que estava escondido / mensagem de erro
      }
    });

    this.kanban.newCommentOutput.subscribe(async ({jobId, commentary}) => {
      try {
        let obj: CreateCommentaryResponse = await this.jobService.commentInJob(jobId, commentary).toPromise();
        this.kanban.appendCommentaryOutput.emit(obj.data);
      } catch (err) {
        // TODO: voltar o item que estava escondido / mensagem de erro
      }
    });
  }

  async getJobs(hideOnLoading: boolean = true): Promise<void> {
    this.isLoadingJob = hideOnLoading;
    let adminJob: GetAdminJobsResponse = await this.jobService.getJobs({
      order_by: this.orderBy,
      companies: this.selectedCompanies,
      freelancers: this.selectedFreelancers
    }).toPromise();

    this.job = adminJob.data;
    this.isLoadingJob = false;

    this.cdr.markForCheck();
  }

}
