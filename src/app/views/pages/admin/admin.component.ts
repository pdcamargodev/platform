import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Admin } from '../../../core/_base/crud/models/admin';
import { Router, ActivatedRoute, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { AuthService } from '../../../core/_services';

@Component({
  selector: 'kt-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  private unsubscribe: Subscription[] = [];
  currentPage: string = "geral";
  admin: Admin = null;

  validUrl: string[] = ['geral', 'prestadores', 'empresas', 'produtos', 'planos', 'pacotes', 'relatorios'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private authService: AuthService
  ) {
    this.admin = authService.getCurrentUser().data;
    this.currentPage = this.route.snapshot.paramMap.get('page');

    if(!this.validUrl.includes(this.currentPage)) {
      this.router.navigateByUrl('/administrador');
    }

    let routerSubs = router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });

    this.unsubscribe.push(routerSubs);
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.unsubscribe.forEach(subs => subs.unsubscribe());
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationEnd) {
      this.currentPage = this.route.snapshot.paramMap.get('page');
    }
  }
}
