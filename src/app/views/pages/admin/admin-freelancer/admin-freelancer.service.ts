import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FilterFreelancerDto } from '../../../../core/_base/crud/models/requests';
import { GetFreelancerResponse, BaseResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminFreelancerService {

  constructor(private http: HttpClient) { }

  getFreelancers(filterFreelancerDto: FilterFreelancerDto): Observable<GetFreelancerResponse> {
    return this.http.post<GetFreelancerResponse>(environment.routes.admin.freelancer.get, filterFreelancerDto);
  }

  softDeleteFreelancer(freelancerId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.freelancer.soft_delete.replace(':freelancerId', freelancerId.toString()));
  }

  updateFreelancer(freelancerId: number, payload: object): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.freelancer.update.replace(':freelancerId', freelancerId.toString()), payload);
  }
}
