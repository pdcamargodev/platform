import { Component, OnInit, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { Freelancer } from '../../../../../core/_base/crud/models/freelancer';
import { AdminFreelancerService } from '../admin-freelancer.service';

@Component({
  selector: 'kt-dialog-edit-freelancer',
  templateUrl: './dialog-edit-freelancer.component.html',
  styleUrls: ['./dialog-edit-freelancer.component.scss']
})
export class DialogEditFreelancerComponent implements OnInit, OnDestroy {
  // Public Params
  editFreelancerForm: FormGroup;
  loading: boolean = false;

  @Input() freelancer: Freelancer;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    private titleService: Title,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private adminFreelancerService: AdminFreelancerService
  ) {
    titleService.setTitle('Editar prestador | _mybrief');
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  private initForm(): void {
    this.editFreelancerForm = this.fb.group({
			name: [this.freelancer.profile.name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      lastName: [this.freelancer.profile.last_name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      birthday: [this.freelancer.profile.birthday, Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ])],
      job: [this.freelancer.profile.job, Validators.compose([
        Validators.maxLength(255)
      ])],
      area: [this.freelancer.profile.area[0], Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
      cellphone: [this.freelancer.profile.cellphone, Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
      location: [this.freelancer.profile.location, Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
      email: [this.freelancer.email, Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])],
      monthEarning: [this.freelancer.financial.month_earning, Validators.compose([
        Validators.required,
        Validators.min(0)
      ])],
      hourlyValue: [this.freelancer.financial.current_hourly_value, Validators.compose([
        Validators.required,
        Validators.min(0)
      ])],
      hourlyValueProgress: [this.freelancer.financial.hour_value_progress, Validators.compose([
        Validators.required,
        Validators.min(0)
      ])],
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.editFreelancerForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
  }

  async submit() {
    if(this.loading) {
      return
    }

    const controls = this.editFreelancerForm.controls;
		/** check form */
		if (this.editFreelancerForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    this.loading = true;

    const payload = {
			name: controls['name'].value,
      last_name: controls['lastName'].value,
      cellphone: controls['cellphone'].value,
      job: controls['job'].value,
      area: controls['area'].value,
      location: controls['location'].value,
      birthday: controls['birthday'].value,
      email: controls['email'].value,
      month_earning: parseFloat(controls['monthEarning'].value),
      hour_value_progress: parseFloat(controls['hourlyValueProgress'].value),
      current_hourly_value: parseFloat(controls['hourlyValue'].value)
    };

    if(payload.month_earning == this.freelancer.financial.month_earning) {
      delete payload['month_earning'];
    }

    if(payload.hour_value_progress == this.freelancer.financial.hour_value_progress) {
      delete payload['hour_value_progress'];
    }

    if(payload.current_hourly_value == this.freelancer.financial.current_hourly_value) {
      delete payload['current_hourly_value'];
    }

    if(payload.name == this.freelancer.profile.name) {
      delete payload['name'];
    }

    if(payload.last_name == this.freelancer.profile.last_name) {
      delete payload['last_name'];
    }

    if(payload.cellphone == this.freelancer.profile.cellphone) {
      delete payload['cellphone'];
    }

    if(payload.job == this.freelancer.profile.job) {
      delete payload['job'];
    }

    if(payload.area == this.freelancer.profile.area) {
      delete payload['area'];
    }

    if(payload.location == this.freelancer.profile.location) {
      delete payload['location'];
    }

    if(payload.birthday == this.freelancer.profile.birthday) {
      delete payload['birthday'];
    }

    if(payload.email == this.freelancer.email) {
      delete payload['email'];
    }

    if(Object.keys(payload).length == 0) {
      this.loading = false;
      this.snackBar.open('Prestador atualizado com sucesso', 'Entendi', {
        duration: 3000
      });

      return;
    }

    let {error} = await this.adminFreelancerService.updateFreelancer(this.freelancer.id, payload).toPromise();

    this.snackBar.open(error? 'Erro ao atualizar as informações do prestador' : 'Prestador atualizado com sucesso', 'Entendi', {
      duration: 3000
    });

    this.loading = false;

    if(payload.name != null) {
      this.freelancer.profile.name = payload.name;
    }

    if(payload.last_name != null) {
      this.freelancer.profile.last_name = payload.last_name;
    }

    if(payload.cellphone != null) {
      this.freelancer.profile.cellphone = payload.cellphone;
    }

    if(payload.job != null) {
      this.freelancer.profile.job = payload.job;
    }

    if(payload.area != null) {
      this.freelancer.profile.area = payload.area;
    }

    if(payload.location != null) {
      this.freelancer.profile.location = payload.location;
    }

    if(payload.birthday != null) {
      this.freelancer.profile.birthday = payload.birthday;
    }

    if(payload.email != null) {
      this.freelancer.email = payload.email;
    }

    this.cdr.markForCheck();
  }
}
