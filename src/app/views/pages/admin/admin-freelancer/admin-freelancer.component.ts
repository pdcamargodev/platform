import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Freelancer } from '../../../../core/_base/crud/models/freelancer';
import { DialogEditFreelancerComponent } from './dialog-edit-freelancer/dialog-edit-freelancer.component';
import { DialogFreelancerNewTaskComponent } from './dialog-freelancer-new-task/dialog-freelancer-new-task.component';
import { AdminFreelancerService } from './admin-freelancer.service';
import { GetFreelancerResponse } from '../../../../core/_base/crud/models/responses';
import { MatSnackBar } from '@angular/material';

import Swal from 'sweetalert2';

@Component({
  selector: 'kt-admin-freelancer',
  templateUrl: './admin-freelancer.component.html',
  styleUrls: ['./admin-freelancer.component.scss']
})
export class AdminFreelancerComponent implements OnInit {
  freelancers: Freelancer[] = [];
  isLoading: boolean = false;

  freelancerBeingDeleted: number[] = [];

  private modalRef: NgbModalRef;

  constructor(
    private modalService: NgbModal,
    private readonly adminFreelancerService: AdminFreelancerService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getFreelancers();
  }

  async deleteFreelancer(freelancerId: number) {
    Swal.fire({
      icon: 'warning',
      title: 'Atenção',
      html: 'Você tem certeza que deseja excluir o(a) prestador(a) <b>'+this.freelancers[this.freelancers.findIndex(f => f.id == freelancerId)].profile.name+'</b>? Esta ação é irreversível e o prestador não será mais capaz de acessar a plataforma.',
      reverseButtons: true,
      showCancelButton: true,
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.callDeleteFunction(freelancerId);
      }
    });
  }

  async callDeleteFunction(freelancerId: number) {
    this.freelancerBeingDeleted.push(freelancerId);
    let {error} = await this.adminFreelancerService.softDeleteFreelancer(freelancerId).toPromise();

    this.freelancerBeingDeleted.splice(this.freelancerBeingDeleted.findIndex(id => id == freelancerId), 1);

    this.snackBar.open(error ? 'Erro ao excluir o prestador' : 'Prestador excluído com sucesso', 'Entendi', {
      duration: 3000
    });

    if(!error) {
      this.freelancers.splice(this.freelancers.findIndex(f => f.id == freelancerId), 1);
    }


    this.cdr.markForCheck();
  }

  async getFreelancers() {
    this.isLoading = true;

    let response: GetFreelancerResponse = await this.adminFreelancerService.getFreelancers(null).toPromise();

    this.freelancers = response.data;
    this.isLoading = false;
    this.cdr.markForCheck();
  }

  openEditFreelancerDialog(freelancer: Freelancer): void {
    this.modalRef = this.modalService.open(DialogEditFreelancerComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.freelancer = freelancer;
  }

  openFreelancerNewTaskDialog(freelancer: Freelancer): void {
    this.modalRef = this.modalService.open(DialogFreelancerNewTaskComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.freelancer = freelancer;
  }

}
