import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogEditPlanComponent } from './dialog-edit-plan/dialog-edit-plan.component';
import { Plan } from '../../../../core/_base/crud/models/plan';
import { AdminPlanService } from './admin-plan.service';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'kt-admin-plan',
  templateUrl: './admin-plan.component.html',
  styleUrls: ['./admin-plan.component.scss']
})
export class AdminPlanComponent implements OnInit {
  // Private params
  private modalRef: NgbModalRef;

  // Public params
  public isLoading: boolean = false;
  public plans: Plan[] = [];

  public planBeingDeleted: number[] = [];

  constructor(
    private modalService: NgbModal,
    private readonly adminPlanService: AdminPlanService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getPlans();
  }

  private async getPlans() {
    this.isLoading = true;

    this.plans = (await this.adminPlanService.getPlans(null).toPromise()).data;

    this.isLoading = false;

    this.cdr.markForCheck();
  }

  openEditPlanDialog(plan: Plan): void {
    this.modalRef = this.modalService.open(DialogEditPlanComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.plan = plan;
  }

  async deletePlan(planId: number) {
    Swal.fire({
      icon: 'warning',
      title: 'Atenção',
      html: 'Você tem certeza que deseja excluir o plano <b>'+this.plans[this.plans.findIndex(f => f.id == planId)].name+'</b>? Esta ação é irreversível e nenhuma empresa poderá mais comprar este plano.',
      reverseButtons: true,
      showCancelButton: true,
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.callDeleteFunction(planId);
      }
    });
  }

  async callDeleteFunction(planId: number) {
    this.planBeingDeleted.push(planId);
    let {error} = await this.adminPlanService.softDeletePlan(planId).toPromise();

    this.planBeingDeleted.splice(this.planBeingDeleted.findIndex(id => id == planId), 1);

    this.snackBar.open(error ? 'Erro ao excluir o plano' : 'Plano excluído com sucesso', 'Entendi', {
      duration: 3000
    });

    if(!error) {
      this.plans.splice(this.plans.findIndex(f => f.id == planId), 1);
    }


    this.cdr.markForCheck();
  }
}
