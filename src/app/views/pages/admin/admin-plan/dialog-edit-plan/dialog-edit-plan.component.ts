import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AdminPlanService } from '../admin-plan.service';
import { Subscription } from 'rxjs';
import { Plan } from '../../../../../core/_base/crud/models/plan';

@Component({
  selector: 'kt-dialog-edit-plan',
  templateUrl: './dialog-edit-plan.component.html',
  styleUrls: ['./dialog-edit-plan.component.scss']
})
export class DialogEditPlanComponent implements OnInit {

  // Public Params
  editPlanForm: FormGroup;
  loading: boolean = false;

  @Input() plan: Plan;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private adminPlanService: AdminPlanService
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  private initForm(): void {
    this.editPlanForm = this.fb.group({
			name: [this.plan.name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      description: [this.plan.description],
      allowedPaymentType: [this.plan.allowed_payment_type, Validators.compose([
        Validators.required
      ])]
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.editPlanForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
  }

  async submit() {
    if(this.loading) {
      return
    }

    const controls = this.editPlanForm.controls;
		/** check form */
		if (this.editPlanForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    const payload = {
      name: controls['name'].value,
      description: controls['description'].value,
      allowed_payment_type: controls['allowedPaymentType'].value,
    }

    if(payload.name == this.plan.name) {
      delete payload['name'];
    }

    if(payload.description == this.plan.description) {
      delete payload['description'];
    }

    if(payload.allowed_payment_type == this.plan.allowed_payment_type) {
      delete payload['allowed_payment_type'];
    }

    this.loading = true;

    if(Object.keys(payload).length == 0) {
      this.loading = false;
      this.snackBar.open('Plano atualizado com sucesso', 'Entendi', {
        duration: 3000
      });

      return;
    }

    let {error} = await this.adminPlanService.updatePlan(this.plan.id, payload).toPromise();

    this.snackBar.open(error? 'Erro ao atualizar as informações do plano' : 'Plano atualizado com sucesso', 'Entendi', {
      duration: 3000
    });

    this.loading = false;

    this.cdr.markForCheck();
  }

}
