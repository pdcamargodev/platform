import { Injectable } from '@angular/core';
import { GetPlansResponse, BaseResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminPlanService {

  constructor(private http: HttpClient) { }

  getPlans(payload): Observable<GetPlansResponse> {
    return this.http.post<GetPlansResponse>(environment.routes.admin.plan.get, payload);
  }

  softDeletePlan(planId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.plan.delete.replace(':planId', planId.toString()));
  }

  updatePlan(planId: number, payload: any): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.plan.update.replace(':planId', planId.toString()), payload)
  }
}
