import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Company } from '../../../../core/_base/crud/models/company';
import { DialogEditCompanyComponent } from './dialog-edit-company/dialog-edit-company.component';
import { DialogCompanyRequestComponent } from './dialog-company-request/dialog-company-request.component';
import { AdminCompanyService } from './admin-company.service';
import { MatSnackBar } from '@angular/material';

import Swal from 'sweetalert2';

@Component({
  selector: 'kt-admin-company',
  templateUrl: './admin-company.component.html',
  styleUrls: ['./admin-company.component.scss']
})
export class AdminCompanyComponent implements OnInit {
  private modalRef: NgbModalRef;
  isLoading: boolean = false;
  companies: Company[] = [];

  companiesBeingDeleted: number[] = [];

  constructor(
    private modalService: NgbModal,
    private readonly adminCompanyService: AdminCompanyService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getCompanies();
  }

  async getCompanies() {
    this.isLoading = true;

    this.companies = (await this.adminCompanyService.getCompanies(null).toPromise()).data;

    this.isLoading = false;
    this.cdr.markForCheck();
  }

  openEditCompanyDialog(company: Company): void {
    this.modalRef = this.modalService.open(DialogEditCompanyComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.company = company;
  }

  openCompanyRequestDialog(company: Company): void {
    this.modalRef = this.modalService.open(DialogCompanyRequestComponent, {
      centered: true,
      size: 'lg'
    });

    this.modalRef.componentInstance.company = company;
  }

  async deleteCompany(companyId: number) {
    Swal.fire({
      icon: 'warning',
      title: 'Atenção',
      html: 'Você tem certeza que deseja excluir a empresa <b>'+this.companies[this.companies.findIndex(f => f.id == companyId)].name+'</b>? Esta ação é irreversível e nenhum usuário da empresa será mais capaz de acessar a plataforma.',
      reverseButtons: true,
      showCancelButton: true,
      cancelButtonText: 'Não, cancelar',
      confirmButtonText: 'Sim, excluir',
      buttonsStyling: false,
      customClass: {
        confirmButton: 'btn btn-danger',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.callDeleteFunction(companyId);
      }
    });
  }

  async callDeleteFunction(companyId: number) {
    this.companiesBeingDeleted.push(companyId);
    let {error} = await this.adminCompanyService.softDeleteCompany(companyId).toPromise();

    this.companiesBeingDeleted.splice(this.companiesBeingDeleted.findIndex(id => id == companyId), 1);

    this.snackBar.open(error ? 'Erro ao excluir a empresa' : 'Empresa excluída com sucesso', 'Entendi', {
      duration: 3000
    });

    if(!error) {
      this.companies.splice(this.companies.findIndex(f => f.id == companyId), 1);
    }


    this.cdr.markForCheck();
  }
}
