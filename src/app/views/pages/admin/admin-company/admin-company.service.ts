import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetCompaniesResponse, BaseResponse } from '../../../../core/_base/crud/models/responses';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminCompanyService {

  constructor(private http: HttpClient) { }

  getCompanies(payload): Observable<GetCompaniesResponse> {
    return this.http.post<GetCompaniesResponse>(environment.routes.admin.company.get, payload);
  }

  updateCompany(companyId: number, payload: any): Observable<BaseResponse> {
    return this.http.put<BaseResponse>(environment.routes.admin.company.update.replace(':companyId', companyId.toString()), payload);
  }

  softDeleteCompany(companyId: number): Observable<BaseResponse> {
    return this.http.delete<BaseResponse>(environment.routes.admin.company.delete.replace(':companyId', companyId.toString()));
  }
}
