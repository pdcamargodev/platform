import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import { Company } from '../../../../../core/_base/crud/models/company';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AdminCompanyService } from '../admin-company.service';

@Component({
  selector: 'kt-dialog-edit-company',
  templateUrl: './dialog-edit-company.component.html',
  styleUrls: ['./dialog-edit-company.component.scss']
})
export class DialogEditCompanyComponent implements OnInit, OnDestroy {
  // Public Params
  public editCompanyForm: FormGroup;
  public loading: boolean = false;

  @Input('company')
  public company: Company;

  // Private params
  private subscriptions: Subscription[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private snackBar: MatSnackBar,
    private titleService: Title,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private adminCompanyService: AdminCompanyService
  ) {
    titleService.setTitle('Editar empresa | _mybrief');
  }

  public ngOnInit() {
    this.initForm();
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }

  async submit() {
    if(this.loading) {
      return
    }

    const controls = this.editCompanyForm.controls;
		/** check form */
		if (this.editCompanyForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }

    this.loading = true;

    const payload = {
      name: controls['name'].value,
      segment: controls['segment'].value,
      cnpj: controls['cnpj'].value,
      localization: controls['localization'].value,
      cellphone: controls['cellphone'].value,
      telephone: controls['telephone'].value,
      site: controls['site'].value
    }

    if(payload.name == this.company.name) {
      delete payload['name'];
    }

    if(payload.segment == this.company.segment) {
      delete payload['segment'];
    }

    if(payload.cnpj == this.company.cnpj) {
      delete payload['cnpj'];
    }

    if(payload.localization == this.company.localization) {
      delete payload['localization'];
    }

    if(payload.cellphone == this.company.cellphone) {
      delete payload['cellphone'];
    }

    if(payload.telephone == this.company.telephone) {
      delete payload['telephone'];
    }

    if(payload.site == this.company.site) {
      delete payload['site'];
    }

    if(Object.keys(payload).length == 0) {
      this.loading = false;
      this.snackBar.open('Empresa atualizada com sucesso', 'Entendi', {
        duration: 3000
      });

      return;
    }

    let {error} = await this.adminCompanyService.updateCompany(this.company.id, payload).toPromise();

    this.snackBar.open(error? 'Erro ao atualizar as informações da empresa' : 'Empresa atualizada com sucesso', 'Entendi', {
      duration: 3000
    });

    this.loading = false;
  }

  public isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.editCompanyForm.controls[controlName];
		if (!control) {
			return false;
    }
		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
  }

  private initForm(): void {
    this.editCompanyForm = this.fb.group({
			name: [this.company.name, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      segment: [this.company.segment, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      cnpj: [this.company.cnpj, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      localization: [this.company.localization, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      cellphone: [this.company.cellphone, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      telephone: [this.company.telephone, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])],
      site: [this.company.site, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255)
      ])]
    });

  }
}
