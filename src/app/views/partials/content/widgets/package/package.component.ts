import { Component, AfterViewInit, Input } from '@angular/core';
import { PackageProduct } from './../../../../../core/_base/crud/models';

type AllowedPaymentType = 'all'|'credit_card'|'bank_slip';

@Component({
  selector: 'kt-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements AfterViewInit {
	@Input('class') class: string = "";
	@Input('name') name: string;
	@Input('description') description: string;
	@Input('allowedPaymentType') allowedPaymentType: AllowedPaymentType;
	@Input('products') packageProducts: PackageProduct[] = [];

	@Input('image') image: string;

	@Input('cash') cash: number;
	@Input('coin') coin: number;

	constructor() { }

	ngAfterViewInit() {
	}

}
