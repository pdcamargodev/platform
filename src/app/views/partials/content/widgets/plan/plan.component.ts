import { Component, OnInit, Input } from '@angular/core';
import { PlanProduct } from './../../../../../core/_base/crud/models';

type AllowedPaymentType = 'all'|'credit_card'|'bank_slip';

@Component({
  selector: 'kt-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
	@Input('class') class: string = "";
	@Input('name') name: string;
	@Input('description') description: string;
	@Input('allowedPaymentType') allowedPaymentType: AllowedPaymentType;
	@Input('products') planProducts: PlanProduct[] = [];

	@Input('image') image: string;

	@Input('cash') cash: number;

	constructor() { }

	ngOnInit() {
	}

}
