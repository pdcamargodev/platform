import { Component, OnInit, Output, EventEmitter, Input, ViewEncapsulation, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import { JobCommentary, JobAttachment, Job } from '../../../../../core/_base/crud/models/job';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

declare let $: any;

import Swal from 'sweetalert2';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'kt-admin-kanban-dialog',
  templateUrl: './admin-kanban-dialog.component.html',
  styleUrls: ['./admin-kanban-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminKanbanDialogComponent implements OnInit, AfterViewInit {
  showDatePicker: boolean = true;
  selectedMoments;

  finishDate: Date;

  uploadingAttachment: JobAttachment[] = [];
  deletingAttachment: number[] = [];

  moment = moment;
  @Input('job') job: Job;
  @Input('attachmentEmitter') attachmentEmitter: EventEmitter<{
    jobId: number,
    formData: FormData,
    skeletonIndex?: number
  }>;
  @Output('removeSkeleton') removeSkeletonOutput: EventEmitter<{
    attachment: JobAttachment,
    skeletonIndex: number
  }> = new EventEmitter<{
    attachment: JobAttachment,
    skeletonIndex: number
  }>();
  @Output('newComment') newCommentOutput: EventEmitter<{jobId: number, commentary: string}>;
  @Output('updateDescription') updateDescriptionOutput: EventEmitter<{jobId: number, description: string}>;
  @Output('updateTitle') updateTitleOutput: EventEmitter<{jobId: number, title: string}>;
  @Output('changeDeadline') changeDeadlineOutput: EventEmitter<{jobId: number, startDate: Date, finishDate: Date}>;
  @Output('updateDeadlineFront') updateDeadlineFrontOutput: EventEmitter<{jobId: number, startDate: Date, finishDate: Date}>;
  @Output('removeAttachmentFromArray') removeAttachmentFromArrayOutput: EventEmitter<number>;
  @Input('deleteAttachmentEmitter') deleteAttachmentEmitter: EventEmitter<number>;
  @Output('appendCommentary') appendCommentaryOutput: EventEmitter<JobCommentary>;

  console = console;

  creatingCommentary: boolean = false;
  config = {
    callbacks: {
      onFocus: () => {
        this.setAsCommenting(true);
      },
      onBlur: () => {
        this.setAsCommenting(false);
      }
    },
    airMode: true,
    placeholder: 'Inserir novo comentário',
    disableDragAndDrop: true,
    popover: {
      air: [
        ['misc', ['undo', 'redo']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol']],
        ['insert', ['link']]
      ]
    },
    hint: {
      mentions: ['mybrief', 'everyone'],
      match: /\B@(\w*)$/,
      search: function (keyword, callback) {
        callback($.grep(this.mentions, function (item: any) {
          return item.indexOf(keyword) == 0;
        }));
      },
      content: function (item: any) {
        return '@' + item;
      }
    }
  }

  descriptionConfig = {
    placeholder: 'Descrição do card',
    disableDragAndDrop: true,
    airMode: true,
    height: 90,
    popover: {
      air: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
      ]
    }
    ,
    callbacks: {
      onChange: function(contents, $editable) {
        console.log('onChange:', contents, $editable);
      }
    }
  }

  constructor(public activeModal: NgbActiveModal, private snackBar: MatSnackBar, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.selectedMoments = [
      this.job.start_date,
      this.job.finish_date
    ];

    $('#summernote').summernote(this.config);
    $('#cardDescription').summernote(this.descriptionConfig);
    $('#cardDescription').summernote('editor.insertText', this.job.notes);
  }

  ngAfterViewInit(): void {
    this.removeSkeletonOutput
      .subscribe(({ attachment, skeletonIndex }) => {
        this.uploadingAttachment.splice(skeletonIndex, 1);
        this.job.attachment.push(attachment);
      });

    this.updateDeadlineFrontOutput
      .subscribe(({ jobId, startDate, finishDate }) => {
        this.job.start_date = startDate;
        this.job.finish_date = finishDate;

        this.cdr.markForCheck();
        this.snackBar.open('Prazo alterado com sucesso', 'Entendi', {
          duration: 3000
        })
      });

    this.removeAttachmentFromArrayOutput
      .subscribe((attachmentId: number) => {
        this.deletingAttachment.splice(this.deletingAttachment.findIndex((id) => id == attachmentId), 1);

        this.job.attachment.splice(this.job.attachment.findIndex((attachment: JobAttachment) => attachment.id == attachmentId), 1);
      });

    this.appendCommentaryOutput
      .subscribe((commentary: JobCommentary) => {
        this.job.commentary.unshift(commentary);
        this.resetCommentary();
      });
  }

  isCommentEmpty(): boolean {
    return $('#summernote').summernote('isEmpty');
  }

  resetCommentary(): void {
    $('#summernote').summernote('reset');
  }

  getCurrentCommentary(): string {
    return $('#summernote').summernote('code');
  }

  getCurrentDescription(): string {
    return $('#cardDescription').summernote('code');
  }

  emitCommentary(): void {
    this.newCommentOutput.emit({
      jobId: this.job.id,
      commentary: this.getCurrentCommentary()
    });
  }

  emitNewDescription(): void {
    this.updateDescriptionOutput.emit({
      jobId: this.job.id,
      description: this.getCurrentDescription()
    });
  }

  emitNewTitle(title: string): void {
    this.updateTitleOutput.emit({
      jobId: this.job.id,
      title: title
    });
  }

  emitDeadline(dates: Date[]): void {
    this.changeDeadlineOutput.emit({
      jobId: this.job.id,
      startDate: dates[0],
      finishDate: dates[1]
    });
  }

  downloadFn(id: number) :string {
    return environment.routes.common.download.attachment.replace(':jobId', id.toString());
  }

  emitDeleteAttachment(attachmentId: number): void {
    if(this.deletingAttachment.includes(attachmentId)) {
      return;
    }

    Swal.fire({
      icon: 'warning',
      title: 'Excluir anexo?',
      html: `
        <div class="text-justify">
          Você deseja excluir este anexo? Esta ação não poderá ser desfeita.
        </div>
      `,
      buttonsStyling: false,
      reverseButtons: true,
      showCancelButton: true,
      confirmButtonText: 'Sim, excluir!',
      cancelButtonText: 'Não, cancelar!',
      customClass: {
        title: 'font-weight-bold',
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.deletingAttachment.push(attachmentId);

        if(this.deleteAttachmentEmitter != null) {
          this.deleteAttachmentEmitter.emit(attachmentId);
        }
      }
    })
   }

  setAsCommenting(commenting: boolean): void {
    this.creatingCommentary = commenting;
  }

  openFileDialog(e: Event): void {
    e.preventDefault();

    let element: HTMLElement = document.getElementById('iptFile');

    element.click();
  }

  onFileChange(event: any): void {
    let file: File = event.target.files[0];

    this.uploadingAttachment.push({
      name: file.name,
      bytes: file.size
    });

    const uploadData: any = new FormData();
    uploadData.append('file', file, file.name);

    if(this.attachmentEmitter != null) {
      this.attachmentEmitter.emit({
        jobId: this.job.id,
        formData: uploadData,
        skeletonIndex: this.uploadingAttachment.length - 1
      });
    }
  }
}
