import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { KanbanDialogComponent } from '../kanban-dialog/kanban-dialog.component';
import { Job, JobAttachment, JobCommentary } from '../../../../../core/_base/crud/models/job';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { Company } from '../../../../../core/_base/crud/models/company';

import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'kt-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.scss']
})
export class KanbanComponent implements OnInit, OnDestroy {
  constructor(
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }
  _jobs: Job[] = [];

  @Input('jobs')
  set jobs(value: Job[]) {
    this._jobs = value;
    this.separateJobsOnCards();
    this.openDialogByUrlParam();
  }

  openDialogByUrlParam(): void {
    this.route.queryParams
      .subscribe(params => {
        if(params.card != null && this._jobs.length > 0) {
          let jobId = parseInt(params.card);
          let href = window.location.href;
          let newHREF = href.replace(window.location.search, "");
          window.history.replaceState({}, document.title, newHREF);
          let job: Job = this._jobs.find((job: Job) => job.id == jobId);

          if(job != null) {
            let actualJob: Job = null;
            if(job.status == 'doing') {
              actualJob = this.doing.find(j => j.id == job.id);
            } else if (job.status == 'done') {
              actualJob = this.done.find(j => j.id == job.id);
            } else if (job.status == 'to_approval') {
              actualJob = this.toApproval.find(j => j.id == job.id);
            } else if (job.status == 'to_change') {
              actualJob = this.toChange.find(j => j.id == job.id);
            } else if (job.status == 'to_do') {
              actualJob = this.toDo.find(j => j.id == job.id);
            }

            if(actualJob != null) {
              this.openDialog(actualJob);
            }
          }
        }
      });
  }

  @Output('attachment') attachmentOutput = new EventEmitter<{ jobId: number, formData: FormData, skeletonIndex?: number}>();
  @Output('removeSkeleton') removeSkeletonOutput = new EventEmitter<{ attachment: JobAttachment, skeletonIndex: number}>();
  @Output('removeAttachmentFromArray') removeAttachmentFromArrayOutput = new EventEmitter<number>();
  @Output('appendCommentary') appendCommentaryOutput = new EventEmitter<JobCommentary>();
  @Output('deleteAttachment') deleteAttachmentOutput = new EventEmitter<number>();
  @Output('newComment') newCommentOutput = new EventEmitter<{jobId: number, commentary: string}>();
  @Output('togglecompany') toggleCompanyOutput = new EventEmitter<number[]>();
  @Output('changeorderby') changeOrderByFilter = new EventEmitter<string>();
  @Output('changejobstatus') changeJobStatus = new EventEmitter<{ jobId: number, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done', oldStatus: 'to_do'|'doing'|'to_approval'|'to_change'|'done', previousIndex: number, currentIndex: number}>();

  @ViewChild('psList', { static: false}) perfectScroll: PerfectScrollbarComponent;

  isFiltering: boolean = false;

  console = console;
  moment = moment;
  maxContainerListHeight: number;

  modalRef: NgbModalRef;

  toDo: Job[] = [];
  doing: Job[] = [];
  toApproval: Job[] = [];
  toChange: Job[] = [];
  done: Job[] = [];

  filteredText: string = "";
  seletectedCompanies: number[] = [];
  uniqueCompanies: Company[] = [];

  ngOnDestroy(): void {
    document.body.style.overflow = null;
  }

  ngOnInit() {
    this.setHeights();
  }

  separateJobsOnCards(): void {
    this.doing = [];
    this.done = [];
    this.toApproval = [];
    this.toChange = [];
    this.toDo = [];

    this._jobs.forEach((job: Job) => {
      document.body.style.overflow = "hidden";
      if(this.uniqueCompanies.find((company: Company) => company.id == job.company.id) == null) {
        this.uniqueCompanies.push(job.company);
      }
      if(job.status == 'doing') {
        this.doing.push(job);
      } else if (job.status == 'done') {
        this.done.push(job);
      } else if(job.status == 'to_approval') {
        this.toApproval.push(job);
      } else if(job.status == 'to_change') {
        this.toChange.push(job);
      } else if(job.status == 'to_do') {
        this.toDo.push(job);
      }
    });
  }

  setHeights(): void {
    this.maxContainerListHeight = window.innerHeight - 210;
    document.getElementById('psList').style.height = this.maxContainerListHeight + 'px';
    document.getElementById('psList').style.maxWidth = window.innerWidth + 'px';

    let containers: NodeListOf<HTMLElement> = document.querySelectorAll('.kt-kanban__list');

    containers.forEach(element => {
      element.style.maxHeight  = (this.maxContainerListHeight - 111) + 'px';
    });
  }

  drop(event: CdkDragDrop<Job[]>, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done') {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else if(status == 'to_approval') {
      Swal.fire({
        title: 'Alterar para aprovação?',
        html: `
          <div class="text-justify">Você deseja enviar para aprovação? Verifique se você colocou tudo que deseja no card antes de enviar a tarefa. Depois que ela for enviada você não conseguirá volta-la para o ambiente de produção sem a validação da _mybrief, bele?</div>
        `,
        icon: 'question',
        showCancelButton: true,
        reverseButtons: true,
        buttonsStyling: false,
        confirmButtonText: 'Sim, enviar!',
        cancelButtonText: 'Não, cancelar!',
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-secondary',
          title: 'font-weight-bold'
        }
      }).then(result => {
        if(result.value) {
          this.transferJobAndEmitEvent(event, status);
        }
      });
    } else {
      this.transferJobAndEmitEvent(event, status);
    }
  }

  transferJobAndEmitEvent(event: CdkDragDrop<Job[]>, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done'): void {
    transferArrayItem(event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex);

      let job: Job = event.container.data[event.currentIndex];

      this.changeJobStatus.emit({
        jobId: job.id,
        status: status,
        oldStatus: job.status,
        previousIndex: event.previousIndex,
        currentIndex: event.currentIndex,
      });

      job.status = status;
      this.cdr.markForCheck();
  }

  replaceSkeletonWithAttachment(attachment: JobAttachment, skeletonIndex: number): void {
    this.removeSkeletonOutput.emit({attachment: attachment, skeletonIndex: skeletonIndex});
  }

  currentItem: Job = null;
  openDialog(item: Job): void {
    this.currentItem = item;
    this.modalRef = this.modalService.open(KanbanDialogComponent, {
      centered: true,
      size: 'lg'
    });
    this.modalRef.componentInstance.job = item;
    this.modalRef.componentInstance.attachmentEmitter = this.attachmentOutput;
    this.modalRef.componentInstance.removeSkeletonOutput = this.removeSkeletonOutput;
    this.modalRef.componentInstance.deleteAttachmentEmitter = this.deleteAttachmentOutput;
    this.modalRef.componentInstance.removeAttachmentFromArrayOutput = this.removeAttachmentFromArrayOutput;
    this.modalRef.componentInstance.newCommentOutput = this.newCommentOutput;
    this.modalRef.componentInstance.appendCommentaryOutput = this.appendCommentaryOutput;
    this.modalRef.result.then((a) => {
        console.log('When user closes', a);
      }, (a) => {
        console.log('Backdrop click', a)
      }
    )
  }

  isLateClassName(finishDate: Date): string {
    return this.isLate(finishDate) ? 'kt-kanban__item--late' : this.isAlmostLate(finishDate) ? 'kt-kanban__item--almost-late' : '';
  }

  finishDateTooltip(job: Job): string {
    if(job.status == 'done') {
      return 'Esta tarefa está concluída';
    }

    let finishDate: Date = job.finish_date;
    let diffHours: number = moment(finishDate).diff(moment(), 'hours');
    let diffMins: number = moment(finishDate).diff(moment(), 'minutes');
    if(this.isAlmostLate(finishDate)) {
      if(diffHours > 0) {
        return `O prazo desta tarefa acaba em ${diffHours} ${diffHours > 1 ? 'horas' : 'hora'}`;
      } else {
        return `O prazo desta tarefa acaba em ${diffMins} ${diffMins > 1 || diffMins == 0 ? 'minutos' : 'minuto'}`;
      }
    } else if(this.isLate(finishDate)) {
      return 'O prazo desta tarefa está atrasado';
    } else {
      return `A tarefa ainda está no prazo`;
    }
  }

  isAlmostLate(finishDate: Date): boolean {
    return moment(finishDate).diff(moment(), 'days') < 1 && !moment(finishDate).isBefore()
  }

  isLate(finishDate: Date): boolean {
    return moment(finishDate).isBefore()
  }

  toggleCompanySelection(id: number): void {
    if(this.seletectedCompanies.includes(id)) {
      this.seletectedCompanies.splice(this.seletectedCompanies.findIndex((companyId: number) => companyId == id), 1);
    } else {
      this.seletectedCompanies.push(id);
    }

    this.toggleCompanyOutput.emit(this.seletectedCompanies);
  }
}
