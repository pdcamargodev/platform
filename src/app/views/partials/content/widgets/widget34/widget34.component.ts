// Angular
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
// Layout
import { LayoutConfigService } from '../../../../../core/_base/layout';
// Charts
import { Chart } from 'chart.js/dist/Chart.min.js';

@Component({
  selector: 'kt-widget34',
  templateUrl: './widget34.component.html',
  styleUrls: ['./widget34.component.scss']
})
export class Widget34Component implements OnInit {
  // Public properties
  @Input() data: { labels: string[]; datasets: any[] };
  @ViewChild('chart', {static: true}) chart: ElementRef;

  /**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService) {
	}

  /**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if (!this.data) {
			this.data = {
				datasets: [{
					data: [10, 20, 30],
					backgroundColor: [
						this.layoutConfigService.getConfig('colors.state.success'),
						this.layoutConfigService.getConfig('colors.state.danger'),
						this.layoutConfigService.getConfig('colors.state.warning')
					]
				}],
				// These labels appear in the legend and in the tooltips when hovering different arcs
				labels: [
					'Entregues',
					'Para aprovar',
					'Em desenvolvimento'
				]
			}
		}

		this.initChartJS();
	}

	/** Init chart */
	initChartJS() {
		// For more information about the chartjs, visit this link
		// https://www.chartjs.org/docs/latest/getting-started/usage.html

		const chart = new Chart(this.chart.nativeElement, {
			type: 'doughnut',
			data: this.data,
			options: {}
		});
	}

}
