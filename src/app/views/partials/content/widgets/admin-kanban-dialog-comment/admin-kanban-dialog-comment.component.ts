import { Component, OnInit, ChangeDetectorRef, Input, ElementRef, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { JobCommentary } from '../../../../../core/_base/crud/models/job/job-commentary.model';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

@Component({
  selector: 'kt-admin-kanban-dialog-comment',
  templateUrl: './admin-kanban-dialog-comment.component.html',
  styleUrls: ['./admin-kanban-dialog-comment.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdminKanbanDialogCommentComponent implements OnInit {

  commentText: string|SafeHtml = "";
  moment = moment;

  @Input('comment') comment: JobCommentary;

  @Output('tagged-user-click') taggedUserClick = new EventEmitter();

  constructor(
    private sanatizer: DomSanitizer,
    private cdr: ChangeDetectorRef,
    private elRef: ElementRef
  ) { }

  ngOnInit() {
    this.parseAndSetComment(this.formatComment(this.comment.commentary));
  }

  regex = /(?:^|\s)(@[a-z0-9]\w*)/gi;

  formatComment(comment: any): string {
    var m: any, res: any[] = [];
    while (m = this.regex.exec(comment)) {
      res.push(m[1]);
    }

    for(let tag of res) {
      comment = comment.replace(tag, `<a data-user="${tag}" class="kt-kanban-dialog-comment__user--tagged" href="/${tag.substr(1)}"><b>${tag}</b></a>`);
    }

    return comment;
  }

  parseAndSetComment(htmlComment: string): void {
    this.commentText = this.sanatizer.bypassSecurityTrustHtml(htmlComment);
    this.cdr.detectChanges();
    this.elRef.nativeElement.querySelectorAll('.kt-kanban-dialog-comment__user--tagged').forEach((element: HTMLElement) => {
      element.addEventListener('click', (e: Event) => {
        e.preventDefault();

        this.taggedUserClick.emit({
          user: element.dataset.user
        });
      });
    });
  }

}
