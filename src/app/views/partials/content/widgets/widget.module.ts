import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, MatDialogModule, MatTooltipModule, MatInputModule, MatDatepickerModule, } from '@angular/material';
import { CoreModule } from '../../../../core/core.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import {DragDropModule} from '@angular/cdk/drag-drop';

// General widgets
import { Widget1Component } from './widget1/widget1.component';
import { Widget4Component } from './widget4/widget4.component';
import { Widget5Component } from './widget5/widget5.component';
import { Widget12Component } from './widget12/widget12.component';
import { Widget14Component } from './widget14/widget14.component';
import { Widget26Component } from './widget26/widget26.component';
import { Timeline2Component } from './timeline2/timeline2.component';
import { Widget34Component } from './widget34/widget34.component';
import { KanbanComponent } from './kanban/kanban.component';
import { KanbanDialogComponent } from './kanban-dialog/kanban-dialog.component';
import { NgbModalModule, NgbTooltipModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { KanbanDialogCommentComponent } from './kanban-dialog-comment/kanban-dialog-comment.component';

import { ReactiveFormsModule } from '@angular/forms';
import { AdminKanbanComponent } from './admin-kanban/admin-kanban.component';
import { AdminKanbanDialogComponent } from './admin-kanban-dialog/admin-kanban-dialog.component';
import { AdminKanbanDialogCommentComponent } from './admin-kanban-dialog-comment/admin-kanban-dialog-comment.component';

import { FormsModule } from '@angular/forms';

import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl} from 'ng-pick-datetime';
import { CompanyRequestsPercentageComponent } from './company-requests-percentage/company-requests-percentage.component';
import { CompanyAvailableCoinsAndPurchasePercentageComponent } from './company-available-coins-and-purchase-percentage/company-available-coins-and-purchase-percentage.component';
import { CompanyRequestsListComponent } from './company-requests-list/company-requests-list.component';
import { CompanyRateRequestComponent } from './company-rate-request/company-rate-request.component';
import { ProductComponent } from './product/product.component';
import { PlanComponent } from './plan/plan.component';
import { PackageComponent } from './package/package.component';
import { StoreFiltersComponent } from './store-filters/store-filters.component';

export class DefaultIntl extends OwlDateTimeIntl {
    /** A label for the up second button (used by screen readers).  */
    upSecondLabel= 'Adicionar um segundo';

    /** A label for the down second button (used by screen readers).  */
    downSecondLabel= 'Menos um segundo';

    /** A label for the up minute button (used by screen readers).  */
    upMinuteLabel= 'Adicionar um minuto';

    /** A label for the down minute button (used by screen readers).  */
    downMinuteLabel= 'Menos um minuto';

    /** A label for the up hour button (used by screen readers).  */
    upHourLabel= 'Adicionar uma hora';

    /** A label for the down hour button (used by screen readers).  */
    downHourLabel= 'Subtrair uma hora';

    /** A label for the previous month button (used by screen readers). */
    prevMonthLabel= 'Mês anterior';

    /** A label for the next month button (used by screen readers). */
    nextMonthLabel= 'Próximo mês';

    /** A label for the previous year button (used by screen readers). */
    prevYearLabel= 'Ano interior';

    /** A label for the next year button (used by screen readers). */
    nextYearLabel= 'Próximo ano';

    /** A label for the previous multi-year button (used by screen readers). */
    prevMultiYearLabel= '21 anos anteriores';

    /** A label for the next multi-year button (used by screen readers). */
    nextMultiYearLabel= 'Próximos 21 anos';

    /** A label for the 'switch to month view' button (used by screen readers). */
    switchToMonthViewLabel= 'Alterar para a visualização por mês';

    /** A label for the 'switch to year view' button (used by screen readers). */
    switchToMultiYearViewLabel= 'Escolher mês e ano';

    /** A label for the cancel button */
    cancelBtnLabel= 'Cancelar';

    /** A label for the set button */
    setBtnLabel= 'Salvar';

    /** A label for the range 'from' in picker info */
    rangeFromLabel= 'Início';

    /** A label for the range 'to' in picker info */
    rangeToLabel= 'Fim';

    /** A label for the hour12 button (AM) */
    hour12AMLabel= 'AM';

    /** A label for the hour12 button (PM) */
    hour12PMLabel= 'PM';
};

@NgModule({
	declarations: [
		// Widgets
		Widget1Component,
		Widget4Component,
		Widget5Component,
		Widget12Component,
		Widget14Component,
		Widget26Component,
		Timeline2Component,
		Widget34Component,
		KanbanComponent,
		KanbanDialogComponent,
		KanbanDialogCommentComponent,
		AdminKanbanComponent,
		AdminKanbanDialogComponent,
		AdminKanbanDialogCommentComponent,
		CompanyRequestsPercentageComponent,
		CompanyAvailableCoinsAndPurchasePercentageComponent,
		CompanyRequestsListComponent,
		CompanyRateRequestComponent,
		ProductComponent,
		PlanComponent,
		PackageComponent,
		StoreFiltersComponent,
	],
	exports: [
		// Widgets
		Widget1Component,
		Widget4Component,
		Widget5Component,
		Widget12Component,
		Widget14Component,
		Widget26Component,
		Timeline2Component,
		Widget34Component,
		KanbanComponent,
		KanbanDialogComponent,
		KanbanDialogCommentComponent,
		AdminKanbanComponent,
		AdminKanbanDialogComponent,
		AdminKanbanDialogCommentComponent,
		CompanyRequestsPercentageComponent,
		CompanyAvailableCoinsAndPurchasePercentageComponent,
		CompanyRequestsListComponent,
		CompanyRateRequestComponent,
		ProductComponent,
		PackageComponent,
		PlanComponent,
		StoreFiltersComponent
	],
	imports: [
		CommonModule,
		PerfectScrollbarModule,
		MatTableModule,
		CoreModule,
		MatIconModule,
		MatTooltipModule,
		ReactiveFormsModule,
    	MatInputModule,
		MatDialogModule,
		MatButtonModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		DragDropModule,
		NgbModalModule,
		NgbTooltipModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		FormsModule,
		NgbProgressbarModule
	],
	entryComponents: [
		KanbanDialogComponent,
		AdminKanbanDialogComponent,
		CompanyRateRequestComponent
	],
	providers: [
        {provide: OwlDateTimeIntl, useClass: DefaultIntl},
    ],
})
export class WidgetModule {
}
