import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

type Types = 'produtos'|'pacotes'|'planos';
type Categories = 'desenvolvimento'|'social-media'|'estrategia';

export type ChangeFilters = {
	type: Types
	category: Categories
	query: string
}

@Component({
  selector: 'kt-store-filters',
  templateUrl: './store-filters.component.html',
  styleUrls: ['./store-filters.component.scss']
})
export class StoreFiltersComponent {
	@Input('initialType') currentType: Types;
	@Input('initialName') currentName: string = "";
	@Input('initialCategory') currentCategory: Categories;

	@Output('changefilters') changeFilters = new EventEmitter<ChangeFilters>();

	setType(type: Types): void {
		this.currentType = type;
	}

	setCategory(category: Categories): void {
		this.currentCategory = category;
	}

	setName(name: string): void {
		this.currentName = name;
	}

	filter(): void {
		this.changeFilters.emit({
			category: this.currentCategory,
			type: this.currentType,
			query: this.currentName
		});
	}
}
