import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Job, JobCommentary, JobAttachment } from '../../../../../core/_base/crud/models/job';
import { Company } from '../../../../../core/_base/crud/models/company';
import { Freelancer } from '../../../../../core/_base/crud/models/freelancer';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
import { moveItemInArray, CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { AdminKanbanDialogComponent } from '../admin-kanban-dialog/admin-kanban-dialog.component';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

@Component({
  selector: 'kt-admin-kanban',
  templateUrl: './admin-kanban.component.html',
  styleUrls: ['./admin-kanban.component.scss']
})
export class AdminKanbanComponent implements OnInit {
  @Output('attachment') attachmentOutput: EventEmitter<{ jobId: number, formData: FormData, skeletonIndex?: number}> = new EventEmitter<{ jobId: number, formData: FormData, skeletonIndex?: number}>();
  @Output('removeSkeleton') removeSkeletonOutput: EventEmitter<{ attachment: JobAttachment, skeletonIndex: number}> = new EventEmitter<{ attachment: JobAttachment, skeletonIndex: number}>();
  @Output('removeAttachmentFromArray') removeAttachmentFromArrayOutput: EventEmitter<number> = new EventEmitter<number>();
  @Output('appendCommentary') appendCommentaryOutput: EventEmitter<JobCommentary> = new EventEmitter<JobCommentary>();
  @Output('deleteAttachment') deleteAttachmentOutput: EventEmitter<number> = new EventEmitter<number>();
  @Output('newComment') newCommentOutput: EventEmitter<{jobId: number, commentary: string}> = new EventEmitter<{jobId: number, commentary: string}>();
  @Output('changeDeadline') changeDeadlineOutput: EventEmitter<{jobId: number, startDate: Date, finishDate: Date}> = new EventEmitter<{jobId: number, startDate: Date, finishDate: Date}>();
  @Output('updateDeadlineFront') updateDeadlineFrontOutput: EventEmitter<{jobId: number, startDate: Date, finishDate: Date}> = new EventEmitter<{jobId: number, startDate: Date, finishDate: Date}>();
  @Output('togglecompany') toggleCompanyOutput: EventEmitter<number[]> = new EventEmitter<number[]>();
  @Output('togglefreelancer') toggleFreelancerOutput: EventEmitter<number[]> = new EventEmitter<number[]>();
  @Output('changeorderby') changeOrderByFilter: EventEmitter<string> = new EventEmitter<string>();
  @Output('changejobstatus') changeJobStatus: EventEmitter<{ jobId: number, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done', oldStatus: 'to_do'|'doing'|'to_approval'|'to_change'|'done', previousIndex: number, currentIndex: number}> = new EventEmitter<{ jobId: number, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done', oldStatus: 'to_do'|'doing'|'to_approval'|'to_change'|'done', previousIndex: number, currentIndex: number}>();

  isFiltering: boolean = false;

  console = console;
  moment = moment;
  maxContainerListHeight: number;

  modalRef: NgbModalRef;

  toDo: Job[] = [];
  doing: Job[] = [];
  toApproval: Job[] = [];
  toChange: Job[] = [];
  done: Job[] = [];

  filteredText: string = "";
  seletectedCompanies: number[] = [];
  selectedFreelancers: number[] = [];
  uniqueCompanies: Company[] = [];
  uniqueFreelancers: Freelancer[] = [];

  constructor(
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }
  _jobs: Job[] = [];

  @Input('jobs')
  set jobs(value: Job[]) {
    this._jobs = value;
    this.separateJobsOnCards();
    this.openDialogByUrlParam();
  }

  openDialogByUrlParam(): void {
    this.route.queryParams
      .subscribe(params => {
        if(params.card != null && this._jobs.length > 0) {
          let jobId = parseInt(params.card);
          let href = window.location.href;
          let newHREF = href.replace(window.location.search, "");
          window.history.replaceState({}, document.title, newHREF);
          let job: Job = this._jobs.find((job: Job) => job.id == jobId);

          if(job != null) {
            let actualJob: Job = null;
            if(job.status == 'doing') {
              actualJob = this.doing.find(j => j.id == job.id);
            } else if (job.status == 'done') {
              actualJob = this.done.find(j => j.id == job.id);
            } else if (job.status == 'to_approval') {
              actualJob = this.toApproval.find(j => j.id == job.id);
            } else if (job.status == 'to_change') {
              actualJob = this.toChange.find(j => j.id == job.id);
            } else if (job.status == 'to_do') {
              actualJob = this.toDo.find(j => j.id == job.id);
            }

            if(actualJob != null) {
              this.openDialog(actualJob);
            }
          }
        }
      });
  }

  separateJobsOnCards(): void {
    this.doing = [];
    this.done = [];
    this.toApproval = [];
    this.toChange = [];
    this.toDo = [];

    this._jobs.forEach((job: Job) => {
      if(this.uniqueCompanies.find((company: Company) => company.id == job.company.id) == null) {
        this.uniqueCompanies.push(job.company);
      }

      if(this.uniqueFreelancers.find((freelancer: Freelancer) => freelancer.id == job.freelancer.id) == null) {
        this.uniqueFreelancers.push(job.freelancer);
      }

      if(job.status == 'doing') {
        this.doing.push(job);
      } else if (job.status == 'done') {
        this.done.push(job);
      } else if(job.status == 'to_approval') {
        this.toApproval.push(job);
      } else if(job.status == 'to_change') {
        this.toChange.push(job);
      } else if(job.status == 'to_do') {
        this.toDo.push(job);
      }
    });
  }

  setHeights(): void {
    this.maxContainerListHeight = window.innerHeight - 210;
    document.getElementById('psList').style.height = this.maxContainerListHeight + 'px';
    document.getElementById('psList').style.maxWidth = window.innerWidth + 'px';

    let containers: NodeListOf<HTMLElement> = document.querySelectorAll('.kt-admin-kanban__list');

    containers.forEach(element => {
      element.style.maxHeight  = (this.maxContainerListHeight - 111) + 'px';
    });
  }

  currentItem: Job = null;
  openDialog(item: Job): void {
    this.currentItem = item;
    this.modalRef = this.modalService.open(AdminKanbanDialogComponent, {
      centered: true,
      size: 'lg'
    });
    this.modalRef.componentInstance.job = item;
    this.modalRef.componentInstance.attachmentEmitter = this.attachmentOutput;
    this.modalRef.componentInstance.removeSkeletonOutput = this.removeSkeletonOutput;
    this.modalRef.componentInstance.deleteAttachmentEmitter = this.deleteAttachmentOutput;
    this.modalRef.componentInstance.removeAttachmentFromArrayOutput = this.removeAttachmentFromArrayOutput;
    this.modalRef.componentInstance.newCommentOutput = this.newCommentOutput;
    this.modalRef.componentInstance.changeDeadlineOutput = this.changeDeadlineOutput;
    this.modalRef.componentInstance.appendCommentaryOutput = this.appendCommentaryOutput;
    this.modalRef.componentInstance.updateDeadlineFrontOutput = this.updateDeadlineFrontOutput;
    this.modalRef.result.then((a) => {
        console.log('When user closes', a);
      }, (a) => {
        console.log('Backdrop click', a)
      }
    )
  }

  ngOnInit() {
    document.body.style.overflow = "hidden";
    this.setHeights();
  }

  drop(event: CdkDragDrop<Job[]>, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done') {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      this.transferJobAndEmitEvent(event, status);
    }
  }

  transferJobAndEmitEvent(event: CdkDragDrop<Job[]>, status: 'to_do'|'doing'|'to_approval'|'to_change'|'done'): void {
    transferArrayItem(event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex);

      let job: Job = event.container.data[event.currentIndex];

      this.changeJobStatus.emit({
        jobId: job.id,
        status: status,
        oldStatus: job.status,
        previousIndex: event.previousIndex,
        currentIndex: event.currentIndex,
      });

      job.status = status;
      this.cdr.markForCheck();
  }

  isLateClassName(finishDate: Date): string {
    return this.isLate(finishDate) ? 'kt-kanban__item--late' : this.isAlmostLate(finishDate) ? 'kt-kanban__item--almost-late' : '';
  }

  finishDateTooltip(job: Job): string {
    if(job.status == 'done') {
      return 'Esta tarefa está concluída';
    }

    let finishDate: Date = job.finish_date;
    let diffHours: number = moment(finishDate).diff(moment(), 'hours');
    let diffMins: number = moment(finishDate).diff(moment(), 'minutes');
    if(this.isAlmostLate(finishDate)) {
      if(diffHours > 0) {
        return `O prazo desta tarefa acaba em ${diffHours} ${diffHours > 1 ? 'horas' : 'hora'}`;
      } else {
        return `O prazo desta tarefa acaba em ${diffMins} ${diffMins > 1 || diffMins == 0 ? 'minutos' : 'minuto'}`;
      }
    } else if(this.isLate(finishDate)) {
      return 'O prazo desta tarefa está atrasado';
    } else {
      return `A tarefa ainda está no prazo`;
    }
  }

  isAlmostLate(finishDate: Date): boolean {
    return moment(finishDate).diff(moment(), 'days') < 1 && !moment(finishDate).isBefore()
  }

  isLate(finishDate: Date): boolean {
    return moment(finishDate).isBefore()
  }

  toggleCompanySelection(id: number): void {
    if(this.seletectedCompanies.includes(id)) {
      this.seletectedCompanies.splice(this.seletectedCompanies.findIndex((companyId: number) => companyId == id), 1);
    } else {
      this.seletectedCompanies.push(id);
    }

    this.toggleCompanyOutput.emit(this.seletectedCompanies);
  }

  toggleFreelancerSelection(id: number): void {
    if(this.selectedFreelancers.includes(id)) {
      this.selectedFreelancers.splice(this.selectedFreelancers.findIndex((freelancerId: number) => freelancerId == id), 1);
    } else {
      this.selectedFreelancers.push(id);
    }

    this.toggleFreelancerOutput.emit(this.selectedFreelancers);
  }
}
