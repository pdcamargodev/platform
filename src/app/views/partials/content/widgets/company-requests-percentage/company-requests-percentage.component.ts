import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { LayoutConfigService } from '../../../../../core/_base/layout';
import { CompanyRequestsPercentage } from '../../../../../core/_base/crud/models/responses';

@Component({
  selector: 'kt-company-requests-percentage',
  templateUrl: './company-requests-percentage.component.html',
  styleUrls: ['./company-requests-percentage.component.scss']
})
export class CompanyRequestsPercentageComponent implements OnInit {

  // Public properties
  chartData: any;
  @Input() data: CompanyRequestsPercentage;
  @ViewChild('chart', {static: true}) chart: ElementRef;

  /**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService) {
	}

  /**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
    this.chartData = {
      options: {
        global: {
          responsive: false,
          maintainAspectRatio: true
        }
      },
      datasets: [{
        data: [
          this.data.done,
          this.data.to_approval,
          this.data.doing,
          this.data.to_change,
          this.data.to_do
        ],
        backgroundColor: [
          this.layoutConfigService.getConfig('colors.state.success'),
          this.layoutConfigService.getConfig('colors.state.danger'),
          this.layoutConfigService.getConfig('colors.state.warning'),
          this.layoutConfigService.getConfig('colors.state.danger'),
          this.layoutConfigService.getConfig('colors.state.primary'),
        ]
      }],
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
        'Entregues',
        'Para aprovar',
        'Em desenvolvimento',
        'Para alteração',
        'Para fazer'
      ]
    }

		this.initChartJS();
	}

	/** Init chart */
	initChartJS() {
		// For more information about the chartjs, visit this link
		// https://www.chartjs.org/docs/latest/getting-started/usage.html

		const chart = new Chart(this.chart.nativeElement, {
			type: 'doughnut',
			data: this.chartData,
			options: {}
		});
	}

}
