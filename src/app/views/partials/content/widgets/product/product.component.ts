import { Component, OnInit, Input } from '@angular/core';

type AllowedPaymentType = 'all'|'credit_card'|'bank_slip';

@Component({
  selector: 'kt-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
	@Input('class') class: string = "";
	@Input('name') name: string;
	@Input('description') description: string;
	@Input('allowedPaymentType') allowedPaymentType: AllowedPaymentType;

	@Input('image') image: string;

	@Input('cash') cash: number;
	@Input('coin') coin: number;

	constructor() { }

	ngOnInit() {
	}

}
