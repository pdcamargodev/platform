import { Component, AfterViewInit, Input, ViewEncapsulation, EventEmitter, Output, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import { Card } from '../../../../../core/_base/crud/models/card';
import { Job, JobAttachment, JobCommentary } from '../../../../../core/_base/crud/models/job';

import { FormControl, FormGroup } from '@angular/forms';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

declare let $: any;

import Swal from 'sweetalert2';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'kt-kanban-dialog',
  templateUrl: './kanban-dialog.component.html',
  styleUrls: ['./kanban-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class KanbanDialogComponent implements AfterViewInit, OnInit {
  commentForm: FormGroup;

  uploadingAttachment: JobAttachment[] = [];
  deletingAttachment: number[] = [];

  moment = moment;
  @Input('job') job: Job;
  @Input('attachmentEmitter') attachmentEmitter: EventEmitter<{
    jobId: number,
    formData: FormData,
    skeletonIndex?: number
  }>;
  @Output('removeSkeleton') removeSkeletonOutput = new EventEmitter<{
    attachment: JobAttachment,
    skeletonIndex: number
  }>();
  @Output('newComment') newCommentOutput: EventEmitter<{jobId: number, commentary: string}>;
  @Output('removeAttachmentFromArray') removeAttachmentFromArrayOutput: EventEmitter<number>;
  @Input('deleteAttachmentEmitter') deleteAttachmentEmitter: EventEmitter<number>;
  @Output('appendCommentary') appendCommentaryOutput: EventEmitter<JobCommentary>;

  console = console;

  creatingCommentary = false;
  config = {
    callbacks: {
      onFocus: () => {
        this.setAsCommenting(true);
      },
      onBlur: () => {
        this.setAsCommenting(false);
      }
    },
    airMode: true,
    placeholder: 'Inserir novo comentário',
    disableDragAndDrop: true,
    popover: {
      air: [
        ['misc', ['undo', 'redo']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol']],
        ['insert', ['link']]
      ]
    },
    hint: {
      mentions: ['mybrief', 'everyone'],
      match: /\B@(\w*)$/,
      search: function (keyword, callback) {
        callback($.grep(this.mentions, function (item: any) {
          return item.indexOf(keyword) == 0;
        }));
      },
      content: function (item: any) {
        return '@' + item;
      }
    }
  }

  constructor(public activeModal: NgbActiveModal, private snackBar: MatSnackBar) {
    this.commentForm = new FormGroup({
      html: new FormControl()
    });
  }

  ngOnInit(): void {
    $('#summernote').summernote(this.config);
  }
  ngAfterViewInit(): void {
    this.removeSkeletonOutput
      .subscribe(({ attachment, skeletonIndex }) => {
        this.uploadingAttachment.splice(skeletonIndex, 1);
        this.job.attachment.push(attachment);
      });

    this.removeAttachmentFromArrayOutput
      .subscribe((attachmentId: number) => {
        this.deletingAttachment.splice(this.deletingAttachment.findIndex((id) => id == attachmentId), 1);

        this.job.attachment.splice(this.job.attachment.findIndex((attachment: JobAttachment) => attachment.id == attachmentId), 1);
      });

    this.appendCommentaryOutput
      .subscribe((commentary: JobCommentary) => {
        this.job.commentary.unshift(commentary);
        this.resetCommentary();
      });
  }

  isCommentEmpty(): boolean {
    return $('#summernote').summernote('isEmpty');
  }

  resetCommentary(): void {
    $('#summernote').summernote('reset');
  }

  getCurrentCommentary(): string {
    return $('#summernote').summernote('code');
  }

  emitCommentary(): void {
    this.newCommentOutput.emit({
      jobId: this.job.id,
      commentary: this.getCurrentCommentary()
    });
  }

  downloadFn(id: number) :string {
    return environment.routes.common.download.attachment.replace(':jobId', id.toString());
  }

  emitDeleteAttachment(attachmentId: number): void {
    if(this.deletingAttachment.includes(attachmentId)) {
      return;
    }

    Swal.fire({
      icon: 'warning',
      title: 'Excluir anexo?',
      html: `
        <div class="text-justify">
          Você deseja excluir este anexo? Esta ação não poderá ser desfeita, mas você poderá anexar novamente caso queira, desde que o card esteja em produção ou alteração, bele?
        </div>
      `,
      buttonsStyling: false,
      reverseButtons: true,
      showCancelButton: true,
      confirmButtonText: 'Sim, excluir!',
      cancelButtonText: 'Não, cancelar!',
      customClass: {
        title: 'font-weight-bold',
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-secondary'
      }
    }).then(result => {
      if(result.value) {
        this.deletingAttachment.push(attachmentId);

        if(this.deleteAttachmentEmitter != null) {
          this.deleteAttachmentEmitter.emit(attachmentId);
        }
      }
    })
   }

  setAsCommenting(commenting: boolean): void {
    this.creatingCommentary = commenting;
  }

  openFileDialog(e: Event): void {
    e.preventDefault();

    let element: HTMLElement = document.getElementById('iptFile');

    element.click();
  }

  onFileChange(event: any): void {
    let file: File = event.target.files[0];

    this.uploadingAttachment.push({
      name: file.name,
      bytes: file.size
    });

    const uploadData: any = new FormData();
    uploadData.append('file', file, file.name);

    if(this.attachmentEmitter != null) {
      this.attachmentEmitter.emit({
        jobId: this.job.id,
        formData: uploadData,
        skeletonIndex: this.uploadingAttachment.length - 1
      });
    }
  }
}
