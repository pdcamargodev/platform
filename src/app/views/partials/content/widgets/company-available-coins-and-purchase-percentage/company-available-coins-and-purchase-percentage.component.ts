import { Component, OnInit, Input } from '@angular/core';
import { CompanyAvailableCoinsAndConsumptionData } from '../../../../../core/_base/crud/models/responses';

@Component({
  selector: 'kt-company-available-coins-and-purchase-percentage',
  templateUrl: './company-available-coins-and-purchase-percentage.component.html',
  styleUrls: ['./company-available-coins-and-purchase-percentage.component.scss']
})
export class CompanyAvailableCoinsAndPurchasePercentageComponent implements OnInit {
  @Input() data: CompanyAvailableCoinsAndConsumptionData;

  constructor() { }

  ngOnInit() {
  }

}
