import { CompanyRateRequestComponent } from './../company-rate-request/company-rate-request.component';
import { NgbModal, NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Job } from '../../../../../core/_base/crud/models/job'

enum Tabs {
	TO_DO = 'to_do',
	DOING = 'doing',
	TO_APPROVAL = 'to_approval',
	TO_CHANGE = 'to_change',
	DONE = 'done'
}

interface Props {
	to_do: Job[]
	doing: Job[]
	to_approval: Job[]
	to_change: Job[]
	done: Job[]
}

interface RateJobPayload {
	jobId: number
	requestId: number
	status: 'approval'|'disapproval'
	feedback: string
}

interface RateJobEvent {
	payload: RateJobPayload,
	activeModal: NgbActiveModal
}

@Component({
  selector: 'kt-company-requests-list',
  templateUrl: './company-requests-list.component.html',
  styleUrls: ['./company-requests-list.component.scss']
})
export class CompanyRequestsListComponent implements OnInit {
	@Output('ratejob') onRateJobOutput = new EventEmitter<RateJobEvent>();

	@Input() data: Props;
	tabs = Tabs;
	currentTab = Tabs.TO_APPROVAL;

	modalRef: NgbModalRef;
	private currentItem: Job;

	constructor(
		private modalService: NgbModal,
	) { }

	ngOnInit() {
	}

	setCurrentTab(tab: Tabs): void {
		this.currentTab = tab;
	}

	openRateJobDialog(item: Job): void {
		this.currentItem = item;
		this.modalRef = this.modalService.open(CompanyRateRequestComponent, {
		  centered: true,
		  size: 'lg'
		});
		this.modalRef.componentInstance.job = item;
		this.modalRef.componentInstance.onRateJob = this.onRateJobOutput;
		this.modalRef.result.then((a) => {
			console.log('When user closes', a);
		  }, (a) => {
			console.log('Backdrop click', a)
		  }
		)
	}
}
