import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Job } from './../../../../../core/_base/crud/models/job/job.model';

import { environment } from './../../../../../../environments/environment';

import moment from 'moment-timezone';
import 'moment/locale/pt-br';
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

import Swal from 'sweetalert2';

interface RateJobPayload {
	jobId: number
	requestId: number
	status: 'approval'|'disapproval'
	feedback: string
}

interface RateJobEvent {
	payload: RateJobPayload,
	activeModal: NgbActiveModal
}

@Component({
  selector: 'kt-company-rate-request',
  templateUrl: './company-rate-request.component.html',
  styleUrls: ['./company-rate-request.component.scss']
})
export class CompanyRateRequestComponent implements OnInit {
	moment = moment;

	@Input('job') job: Job;

	@Output('ratejob') onRateJob: EventEmitter<RateJobEvent>;

	constructor(public activeModal: NgbActiveModal) { }

	ngOnInit() {
	}

	emitJobRate(status: 'approval'|'disapproval', feedback: string): void {
		if(!feedback || feedback.trim() === '') {
			Swal.fire({
				title: 'Erro',
				icon: 'warning',
				text: 'Por favor, nos deixe um feedback para que possamos melhorar cada vez mais as nossas entregas.',
				buttonsStyling: false,
				customClass: {
					confirmButton: 'btn btn-primary'
				},
				confirmButtonText: 'Entendi',
			});

			return;
		}

		Swal.fire({
			title: status === 'approval' ? 'Aprovar?' : 'Desaprovar?',
			icon: 'warning',
			html: `
				Você realmente deseja ${status === 'approval' ? 'aprovar' : 'desaprovar'} a atividade

				<div class="p-3 my-2 text-center font-weight-bold bg-light">
					${this.job.title}
				</div>

				Não se esqueça de verificar seu feedback, ele é muito importante para que possamos continuar melhorando nossas entregas.
			`,
			reverseButtons: true,
			buttonsStyling: false,
			customClass: {
				confirmButton: 'btn btn-primary',
				cancelButton: 'btn btn-secondary'
			},
			showCancelButton: true,
			confirmButtonText: `Sim, ${status === 'approval' ? 'aprovar' : 'desaprovar'}.`,
			cancelButtonText: 'Não, cancelar.'
		}).then(result => {
			if(result.value) {
				this.onRateJob.emit({
					payload: {
						jobId: this.job.id,
						status,
						feedback,
						requestId: this.job.company_feedback_request[0].id
					},
					activeModal: this.activeModal
				});
			}
		})
	}

	downloadFn(id: number) :string {
		return environment.routes.common.download.attachment.replace(':jobId', id.toString());
	}
}
