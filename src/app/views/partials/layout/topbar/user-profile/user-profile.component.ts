// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
import { AuthService } from '../../../../../core/_services';
import { Freelancer } from '../../../../../core/_base/crud/models/freelancer';
import { Router } from '@angular/router';
import { Admin } from '../../../../../core/_base/crud/models/admin';

@Component({
	selector: 'kt-user-profile',
	templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {
	// Public properties
	user: Freelancer|Admin = null;
	userType: 'admin'|'freelancer'|'company';

	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	constructor(
		private authService: AuthService,
		private router: Router
	) {
		if(authService.getCurrentUser() != null) {
			this.user = authService.getCurrentUser().data;
			this.userType = authService.getCurrentUser().userType;
		}
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
	}

	/**
	 * Log out
	 */
	logout() {
		let url: string;

		switch(this.userType) {
			case 'admin':
				url = '/auth/admin/entrar';
				break;
			case 'company':
				url = '/auth/empresa/entrar';
				break;
			case 'freelancer':
				url = '/auth/entrar';
				break;
			default:
				url = '/auth/entrar';
				break;
		}
		this.authService.logout();
		this.router.navigateByUrl(url);
	}
}
