import { Router } from '@angular/router';
import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { CartService, CartItem } from './../../../../../core/_services';

@Component({
	selector: 'kt-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent {
	// Public properties

	// Set icon class name
	@Input() icon: string = 'flaticon2-shopping-cart-1';
	@Input() iconType: '' | 'brand';

	// Set true to icon as SVG or false as icon class
	@Input() useSVG: boolean;

	// Set bg image path
	@Input() bgImage: string;

	items: CartItem[] = [];

	cashPrice: number = 0;
	coinPrice: number = 0;

	constructor(
		private cartService: CartService,
		private cdr: ChangeDetectorRef,
		private router: Router
	) {
		this.items = this.cartService.getCartItems();

		this.setPricesValues();

		this.cartService.items.subscribe(items => {
			this.items = items;

			this.setPricesValues();

			this.cdr.markForCheck();
		});
	}

	private setPricesValues() {
		const { cash, coin } = this.cartService.getCartTotalPrice();

		this.cashPrice = cash;
		this.coinPrice = coin;
	}

	setItemQuantity(index: number, quantity: number): void {
		if(quantity <= 0) {
			this.cartService.removeItem(index);
		} else {
			this.cartService.setItemQuantity(index, quantity);
		}
	}

	removeItem(index: number): void {
		this.cartService.removeItem(index);
	}

	toInt(value: string) {
		return Number(value);
	}

	goToCheckout() {
		this.router.navigateByUrl('/empresa/loja/checkout');
	}
}
