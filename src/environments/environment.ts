import routes from './routes';

let baseApiUrl: string = 'localhost';
let protocol: string = 'http';
let port: string = ':8000';

let apiUrl: string = `${protocol}://${baseApiUrl}${port}`;

export const environment = {
	production: false,
	apiUrl: apiUrl,

	routes: routes(apiUrl)
};
