import routes from './routes';

let baseApiUrl: string = 'api.mybrief.com.br';
let protocol: string = 'https';
let port: string = '';

let apiUrl: string = `${protocol}://${baseApiUrl}${port}`;

export const environment = {
	production: true,
	apiUrl: `${protocol}://${baseApiUrl}${port}/`,

	routes: routes(apiUrl)
};
