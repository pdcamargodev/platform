export default function routes(apiUrl: string) {
    return {
        auth: {
            login: {
                freelancer: `${apiUrl}/auth/login/freelancer`,
                admin: `${apiUrl}/auth/login/admin`,
                company: `${apiUrl}/auth/login/company`
            }
        },
        common: {
            bank: {
                list: `${apiUrl}/common/banks`
            },
            download: {
                attachment: `${apiUrl}/common/download/attachment/:jobId`
            },
            upload: {
                attachment: `${apiUrl}/freelancer/job/:jobId/attach`,
                admin: {
                    attachment: `${apiUrl}/admin/job/:jobId/attach`
                }
            }
        },
        admin: {
            job: {
                get: `${apiUrl}/admin/job/list`,
                status: {
                    change: `${apiUrl}/admin/job/status/:jobId`
                },
                attachment: {
                    delete:  `${apiUrl}/admin/job/attachment/:attachmentId`
                },
                commentary: {
                    create: `${apiUrl}/admin/job/:jobId/comment`
                },
                deadline: {
                    change: `${apiUrl}/admin/job/deadline/:jobId`
                }
            },
            freelancer: {
                get: `${apiUrl}/admin/freelancer`,
                soft_delete: `${apiUrl}/admin/freelancer/:freelancerId`,
                update: `${apiUrl}/admin/freelancer/:freelancerId`
            },
            company: {
                get: `${apiUrl}/admin/company`,
                update: `${apiUrl}/admin/company/:companyId`,
                delete: `${apiUrl}/admin/company/:companyId`,
            },
            product: {
                get: `${apiUrl}/admin/product/list`,
                delete: `${apiUrl}/admin/product/:productId`,
                update: `${apiUrl}/admin/product/:productId`,
            },
            plan: {
                get: `${apiUrl}/admin/plan/list`,
                delete: `${apiUrl}/admin/plan/:planId`,
                update: `${apiUrl}/admin/plan/:planId`,
            },
            package: {
                get: `${apiUrl}/admin/package/list`,
                delete: `${apiUrl}/admin/package/:packageId`,
                update: `${apiUrl}/admin/package/:packageId`,
            },
        },
        freelancer: {
            notification: {
                get: `${apiUrl}/freelancer/notifications`
            },
            job: {
                get: `${apiUrl}/freelancer/jobs`,
                latest: `${apiUrl}/freelancer/jobs/latest`,
                status: {
                    change: `${apiUrl}/freelancer/job/status/:jobId`
                },
                attachment: {
                    delete:  `${apiUrl}/freelancer/job/attachment/:attachmentId`
                },
                commentary: {
                    create: `${apiUrl}/freelancer/job/:jobId/comment`
                }
            },
            profile: {
                get: `${apiUrl}/freelancer?scope=`,
                update: `${apiUrl}/freelancer/profile`,
            },
            financial: {
                time_available: {
                    reanalysis: `${apiUrl}/freelancer/request/time-available`,
                    update: `${apiUrl}/freelancer/time-available`,
                },
                bank_account: {
                    update: `${apiUrl}/freelancer/bank-account`,
                }
            },
            password: {
                change: `${apiUrl}/freelancer/password`
            }
        },
        company: {
			address: `${apiUrl}/company/address/billing`,
			invoice: {
				create: `${apiUrl}/company/invoice`,
			},
            job: {
				feedback: `${apiUrl}/company/job/feedback`,
				list: {
					by_category: `${apiUrl}/company/job/:category`
				},
                request: {
                    percentage: `${apiUrl}/company/request/job/percentage`,
					consumption: `${apiUrl}/company/request/job/consumption`,
                }
			},
			store: {
				product: `${apiUrl}/company/product`,
				package: `${apiUrl}/company/package`,
				plan: `${apiUrl}/company/plan`,
			}
        }
    }
}
